﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:ResponseCaller [EXPORTED]
*|　Author:Jidcoo
*|　Version:1.1
*|　Date:2022-06-28 20:21
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:修改了响应数据的流处理方式，对BodyParser的流处理方式进行了调整优化
*|       将toStreamHandler()方法的返回值类型修改为istringstream
*|  Date:2022-06-28
*-------------------------------------------------------------------------------------------------------------
*/

#include "ResponseCaller.h"

void WSHTTP::ResponseCaller::setHandler(HINTERNET handler)
{
	this->request = handler;
}

WSHTTP::ResponseCaller::ResponseCaller(int code, string msg)
{
	this->code = code;
	this->msg = msg;
	this->request = nullptr;
	this->bodyLen = -1;
}

WSHTTP::ResponseCaller::~ResponseCaller() {
	closeBody();
	responseHeaders.clear();
	responseCookies.clear();
}

int WSHTTP::ResponseCaller::getCode()
{
	return this->code;
}

unsigned long long WSHTTP::ResponseCaller::getBodySize()
{
	if(((int)(this->bodyLen))==-1){
		Util::LOG(L_WARNNING, TAG, "Unknown response body size");
		return 0;
	}
	return this->bodyLen;
}

string WSHTTP::ResponseCaller::getMsg()
{
	return this->msg;
}


void WSHTTP::ResponseCaller::setCode(int _code)
{
	this->code = _code;
}

void WSHTTP::ResponseCaller::setMsg(string _msg)
{
	this->msg = _msg;
}


string WSHTTP::ResponseCaller::toString()
{
	string s("[code=" + to_string(getCode())); s += ",msg=" + getMsg(); s += "]";
	return s;
}

const map<string, string>& WSHTTP::ResponseCaller::getResponseHeaders()
{
	return responseHeaders;
}

const vector<WSHTTP::Cookie>& WSHTTP::ResponseCaller::getResponseCookies()
{
	return responseCookies;
}

WSHTTP::ResponseCaller::BodyParser WSHTTP::ResponseCaller::getBody() {
	return BodyParser::build(this);
}

void WSHTTP::ResponseCaller::closeBody() {
	if (request) {
		InternetCloseHandle(request);
		request = nullptr;
		Util::LOG(L_INFO, "ResponseBodyParser", "HttpRequest closed");
	}
}

void WSHTTP::ResponseCaller::setBodySize(unsigned long long size)
{
	this->bodyLen = size;
	Util::LOG(L_INFO, TAG, "Response body length: " + Util::stringifyFileSize(bodyLen));
}





//////////////////////////////BodyParser/////////////////////////////////////


WSHTTP::ResponseCaller::BodyParser WSHTTP::ResponseCaller::BodyParser::build(ResponseCaller* _cl)
{
	BodyParser parser;
	if (_cl&&_cl->request != nullptr) {
		parser.isReady = true;
		parser.cl = _cl;
		Util::LOG(L_INFO, "ResponseBodyParser", "Get the bodyStream holder successfully");
	}else {
		parser.isReady = false;
		parser.cl = nullptr;
		Util::LOG(L_ERROR, "ResponseBodyParser", "Get the bodyStream holder failed --> HttpRequest closed");
	}
	return parser;
}

WSHTTP::ResponseCaller::BodyParser::~BodyParser()
{
	this->cl = nullptr;
}

string WSHTTP::ResponseCaller::BodyParser::toString()
{
	string result;
	if (cl != nullptr&&isReady&&cl->request) {
		result = toStringHandler();
		result = Util::utf82String(result);
		cl->closeBody();
		cl = nullptr;
		isReady = false;
	}else {
		Util::LOG(L_WARNNING, "ResponseBodyParser", "BodyStream closed");
	}
	return result;
}

istringstream WSHTTP::ResponseCaller::BodyParser::toStream()
{
	istringstream result;
	if (cl != nullptr&&isReady&&cl->request) {
		result = toStreamHandler();
		cl->closeBody();
		cl = nullptr;
		isReady = false;
	}else {
		Util::LOG(L_WARNNING, "ResponseBodyParser", "BodyStream closed");
	}
	return result;
}

bool WSHTTP::ResponseCaller::BodyParser::toFile(ofstream  & s, bool autoClose)
{
	bool result = false;
	if (cl != nullptr&&isReady&&cl->request) {
		if (s.is_open() && s.good()) {
			result = toFileHandler(s, autoClose);
			cl->closeBody();
			cl = nullptr;
		}else {
			Util::LOG(L_ERROR, "ResponseBodyParser", "IO ERROR");
		}
	}
	else {
		Util::LOG(L_WARNNING, "ResponseBodyParser", "BodyStream closed");
	}
	if (autoClose&&s.is_open()) {
		s.close();
	}
	return result;
}

bool WSHTTP::ResponseCaller::BodyParser::toFile(const string & fn, const string & path)
{
	if (path.empty()) {
		Util::LOG(L_ERROR, "ResponseBodyParser", "IO ERROR : the path is not exists");
		return false;
	}
	string _path(path);
	if (_path[_path.size() - 1] != '\\') {
		_path += "\\";
	}
	ofstream  s(_path + fn, ios::binary | ios::out);
	return toFile(s, true);
}

bool WSHTTP::ResponseCaller::BodyParser::toFile(const string & path)
{
	if (cl != nullptr&&isReady&&cl->request) {
	}
	else {
		Util::LOG(L_WARNNING, "ResponseBodyParser", "BodyStream closed before read");
		return false;
	}
	string fn("");
	//Content-Disposition >> Url >> Default Timestamp
	//Content-Disposition [case sensitive]
	map<string, string>::iterator it = this->cl->responseHeaders.find("CONTENT-DISPOSITION");
	if (it != this->cl->responseHeaders.end()) {
		regex reg("filename=\"([^\\\"]*)\\\"", regex_constants::icase);
		cmatch cm;
		regex_search(it->second.c_str(), cm, reg, std::regex_constants::match_default);
		if (cm.size() == 2) { fn = cm[1]; }
	}
	//Url
	if (fn.empty()) {
		if (cl->query.size() > 0) {
			int ri = 0;
			ri = cl->query.find_last_of("/");
			if (ri != -1) { cl->query = cl->query.substr(ri); }
			while ((ri = cl->query.find("/")) != -1) {
				cl->query = cl->query.replace(ri, 1, "");
			}
			if (cl->query.size() > 0) { fn = cl->query; }
		}
	}
	//Default timestamp
	if (fn.empty()) {
		fn = to_string(time(nullptr));
	}
	//handle the suffix
	if (fn.find_last_of(".") == -1) {
		it = this->cl->responseHeaders.find("CONTENT-TYPE");
		if (it != this->cl->responseHeaders.end()) {
			string c = it->second;
			int i = c.find(";");
			if (c.find(";") != -1) {
				c = c.substr(0, i);
			}
			fn = fn + Util::getFileType(c);
		}
	}
	return toFile(fn, path);
}

string WSHTTP::ResponseCaller::BodyParser::toStringHandler()
{
	return toStreamHandler().str();
}

istringstream WSHTTP::ResponseCaller::BodyParser::toStreamHandler()
{
	istringstream out;
	if (cl->request) {
		const int buffLen = 1024;
		BYTE szBuffer[buffLen + 1] = { 0 };
		DWORD dwReadSize = buffLen;
		BOOL bRet = TRUE;
		ostringstream stream;
		while (true) {
			bRet = InternetReadFile(cl->request, szBuffer, buffLen, &dwReadSize);
			if (!bRet || (0 == dwReadSize)) {
				if (GetLastError()) {
					Util::LOG(L_ERROR, cl->TAG, "Read responseStream error:" + to_string(GetLastError()));
				}
				else {
					Util::LOG(L_INFO, cl->TAG, "Read responseStream completed");
				}
				break;
			}
			stream.write((char*)szBuffer, dwReadSize);
		}
		stream.flush();
		out = istringstream(stream.str());
	}
	return out;
}

bool WSHTTP::ResponseCaller::BodyParser::toFileHandler(ofstream  &s, bool autoClose)
{
	bool state = true;
	if (cl->request) {
		unsigned long long CONTENT_LEN = cl->bodyLen;
		Util::LOG(L_INFO, cl->TAG, "Ready to download stream to file. Stream size : " + Util::stringifyFileSize(CONTENT_LEN));
		const int buffLen = 1024;
		BYTE szBuffer[buffLen + 1] = { 0 };
		DWORD dwReadSize = buffLen;
		BOOL bRet = TRUE;
		unsigned long long completed = 0;
		string text = "[0%]";
		int textSize = text.size();
		if(CONTENT_LEN>0){
			Util::LOG(L_INFO, cl->TAG, "Downloading the Stream.......[0%]", false, false);
		}
		while (true) {
			bRet = InternetReadFile(cl->request, szBuffer, buffLen, &dwReadSize);
			if (!bRet || (0 == dwReadSize)) {
				if (GetLastError()) {
					state = false;
					Util::LOG(L_ERROR, cl->TAG, "Read responseStream error:" + to_string(GetLastError()));
				}
				else {
					Util::LOG(L_INFO, cl->TAG, "Read responseStream completed");
				}
				break;
			}
			s.write((char*)szBuffer, dwReadSize);
			completed += dwReadSize;
			if(CONTENT_LEN>0){
				string process = Util::d2string((double)(((double)completed / CONTENT_LEN) * 100), 2);
				if(process=="100.00"){
					text = "[Download Completed][L:" + to_string(CONTENT_LEN) + "][D:" + to_string(completed) + "][100%]";
					Util::ROLLBACK_LOG(L_INFO, cl->TAG, text, textSize, true);
				}else{
					text = "[" + process +"%]";
					Util::ROLLBACK_LOG(L_INFO, cl->TAG, text, textSize, false);
				}
				textSize = text.size();
			}
			if (state) {
				s.flush();
			}
		}

		return state;
	}
	return false;
}
