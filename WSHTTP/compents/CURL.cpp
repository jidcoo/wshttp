﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:CURL class;Parse the string to URL[URL Class][UN-EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2021-09-16 13:03
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "CURL.h"

WSHTTP::CURL::CURL() {}

bool WSHTTP::CURL::isVaild() {
	return vaild;
}

void WSHTTP::CURL::isVaild(bool v)
{
	this->vaild = v;
}

bool WSHTTP::CURL::isSecure()
{
	string HTTPS("HTTPS");
	return getUpperCastScheme().compare(HTTPS) == 0;
}

string WSHTTP::CURL::getScheme()
{
	return this->scheme;
}

string WSHTTP::CURL::getUpperCastScheme()
{
	string s(scheme);
	Util::toUpperString(s);
	return s;

}


void WSHTTP::CURL::setScheme(string val)
{
	this->scheme = val;
}

string WSHTTP::CURL::getUserName()
{
	return this->username;
}

void WSHTTP::CURL::setUserName(string val)
{
	this->username = username;
}

string WSHTTP::CURL::getPassword()
{
	return this->password;
}

void WSHTTP::CURL::setPassword(string val)
{
	this->password = val;
}

string WSHTTP::CURL::getHost()
{
	return this->host;
}

void WSHTTP::CURL::setHost(string val)
{
	this->host = val;
	if (!host.empty()) {
		int separator = host.rfind(":");
		if (separator > -1) {
			setHostName(host.substr(0, separator));
			string part = host.substr(separator + 1, host.length());
			if (!part.empty()) {
				try {
					stringstream ss;
					ss << part;
					int i;
					ss >> i;
					setPort(i);
				}
				catch (...) {

				}
			}
		}
		else {
			setHostName(host);
			setPort(-1);
		}

	}
}

string WSHTTP::CURL::getHostName()
{
	if (!host.empty()) {
		int separator = host.rfind(":");
		if (separator > -1) {
			return host.substr(0, separator);
		}
		return host;
	}
	return "";
}

void WSHTTP::CURL::setHostName(string val)
{
	this->hostname = val;
}


int WSHTTP::CURL::getPort()
{
	return this->port;
}

void WSHTTP::CURL::setPort(int port)
{
	this->port = port;
}

string WSHTTP::CURL::getPath()
{
	return this->path;
}

void WSHTTP::CURL::setPath(string val)
{
	this->path = val;
}

string WSHTTP::CURL::getRawPath()
{
	return this->rawPath;
}

void WSHTTP::CURL::setRawPath(string val)
{
	this->rawPath = val;
}

string WSHTTP::CURL::getQuery()
{
	return this->query;
}

void WSHTTP::CURL::setQuery(string val)
{
	this->query = val;
}

string WSHTTP::CURL::getFragment()
{
	return this->fragment;
}

void WSHTTP::CURL::setFragment(string val)
{
	this->fragment = val;
}

string WSHTTP::CURL::getOpaque()
{
	return this->opaque;
}

void WSHTTP::CURL::setOpaque(string val)
{
	this->opaque = val;
}





bool WSHTTP::CURL::nullOrEmpty(string url)
{
	return url.empty();
}

void WSHTTP::CURL::parseScheme(string & _remaining, string & scheme)
{
	string remaining = string(_remaining);
	int indexColon = remaining.find(':');
	if (indexColon == 0) {
		throw 0;
	}
	if (indexColon < 0) {
		scheme = "";
		_remaining = remaining;
		return;
	}
	char first = remaining.at(0);
	if ('0' <= first && first <= '9' || first == '+' || first == '-' || first == '.') {
		scheme = "";
		_remaining = remaining;
	}
	string _scheme = remaining.substr(0, indexColon);
	Util::toLowerString(_scheme);
	scheme = _scheme;
	_remaining = remaining.substr(indexColon + 1, remaining.length());
}

bool WSHTTP::CURL::isPortValid(string portStr)
{
	if (portStr.empty()) {
		return true;
	}
	int i = portStr.find(':');
	// Port format must be ':8080'
	if (i != 0) {
		return false;
	}
	string segment = portStr.substr(i + 1, portStr.length());
	try {
		stringstream ss;
		ss << segment;
		int i;
		ss >> i;
	}
	catch (...) {
		return false;
	}
	return true;
}

void WSHTTP::CURL::parseUserInfo(string & authority, string & user, string & password)
{
	string str = string(authority);
	int i = str.rfind('@');
	string username;
	string pwd;
	string rest = str;
	if (i >= 0) {
		string credentials = str.substr(0, i);
		if (credentials.find(':') >= 0) {
			vector<string> parts =Util::split(credentials, ":");
			//username = PercentEncoder.decode(parts[0]);
			username = parts[0];//暂时实现，有空就实现PercentEncoder
			//pwd = PercentEncoder.decode(parts[1]);
			pwd = parts[1];//暂时实现，有空就实现PercentEncoder
		}
		else {
			//username = PercentEncoder.decode(credentials);
			username = credentials;//暂时实现，有空就实现PercentEncoder
		}
		rest = str.substr(i + 1, str.length());
	}
	user = username;
	password = pwd;
	authority = rest;
}

void WSHTTP::CURL::parseHost(string & authority, string & host)
{
	string str(authority);
	if (str.length() == 0) {
		host = "";
		return;
	}
	if (str.at(0) == '[') {
		int i = str.rfind(']');
		if (i < 0) {
			throw ("IPv6 detected, but missing closing ']' token");
		}
		string portPart = str.substr(i + 1, str.length());
		if (!isPortValid(portPart)) {
			throw ("invalid port");
		}
	}
	else {
		if (str.find(':') != -1) {
			vector<string> parts = Util::split(str, ":");
			if (parts.size() > 2) {
				throw ("invalid host in: " + str);
			}
			if (parts.size() == 2) {
				try {
					stringstream ss;
					ss << parts[1];
					int i;
					ss >> i;
				}
				catch (...) {
					throw ("invalid port");
				}
			}
		}
	}
	string tmp = str;
	Util::toLowerString(tmp);
	//host = PercentEncoder.decode(tmp);
	host = tmp;
}

string  WSHTTP::CURL::toString()const
{
	string _u;
	_u += hostname;
	_u += path;
	return _u;
}

WSHTTP::CURL* WSHTTP::CURL::parseA(CURL* __url,string _url)
{
	if (__url == nullptr) return __url;
	if (__url->nullOrEmpty(_url)) {
		__url->isVaild(false);
		return __url;
	}else {
		__url->isVaild(true);
		string remaining = _url;
		int index = remaining.rfind('#');
		if (index >= 0) {
			string frag = remaining.substr(index + 1, remaining.length());
			__url->setFragment(frag.empty() ? "" : frag);
			remaining = remaining.substr(0, index);
		}
		if (remaining.empty()) {
			return __url;
		}
		if (strcmp("*", remaining.c_str()) == 0) {
			__url->setPath("*");
			return __url;
		}
		index = remaining.find('?');
		if (index > 0) {
			string query = remaining.substr(index + 1, remaining.length());
			if (query.empty()) {
				__url->setQuery("?");
			}
			else {
				__url->setQuery(query);
			}
			remaining = remaining.substr(0, index);
		}
		string _scheme;
		__url->parseScheme(remaining, _scheme);
		bool hasScheme = !_scheme.empty();
		__url->setScheme(_scheme);
		bool startWith = remaining.find("/") == 0 ? true : false;
		if (hasScheme && !startWith) {
			__url->setOpaque(remaining);
			return __url;
		}
		bool startWith1 = remaining.find("///") == 0 ? true : false;
		bool startWith2 = remaining.find("//") == 0 ? true : false;
		if ((hasScheme || !startWith1 && startWith2)) {
			remaining = remaining.substr(2, remaining.length());
			string authority = remaining;
			int i = remaining.find('/');
			if (i >= 0) {
				authority = remaining.substr(0, i);
				remaining = remaining.substr(i, remaining.length());
			}
			else {
				remaining = "";
			}

			if (!authority.empty()) {
				string userName;
				string pwd;
				__url->parseUserInfo(authority, userName, pwd);
				__url->setUserName(userName);
				__url->setPassword(pwd);
			}
			string _host;
			__url->parseHost(authority, _host);
			__url->setHost(_host);
		}

		if (!remaining.empty()) {
			//url.setPath(PercentEncoder.decode(remaining));
			__url->setPath(remaining);
			__url->setRawPath(remaining);
		}
		return __url;
	}
	return __url;
}

WSHTTP::CURL* WSHTTP::CURL::parse(string  url)
{
	return parseA(new CURL(),url);
}

WSHTTP::CURL WSHTTP::CURL::_parse_(string url)
{
	CURL curl;
	parseA(&curl, url);
	return curl;
}
