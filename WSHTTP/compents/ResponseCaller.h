﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:ResponseCaller [EXPORTED]                                                
*|　Author:Jidcoo                                                
*|　Version:1.1                                                 
*|　Date:2022-06-28 20:21                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:修改了响应数据的流处理方式，对BodyParser的流处理方式进行了调整优化 
*|       将toStreamHandler()方法的返回值类型修改为istringstream
*|  Date:2022-06-28                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once

#include "../framework.h"
#include "Cookie.h"

using namespace std;
namespace WSHTTP {
	class WSHTTP_API ResponseCaller {
	public:
		class WSHTTP_API BodyParser {
		private:
			friend class ResponseCaller;
			ResponseCaller* cl;
			bool isReady;
		private:
			static BodyParser build(ResponseCaller* _cl);
			string toStringHandler();
			istringstream toStreamHandler();
			bool toFileHandler(ofstream &s, bool autoClose);
		public:
			~BodyParser();
			string toString();
			istringstream toStream();
			bool toFile(const string& fn, const string& path);
			bool toFile(const string& path);
			bool toFile(ofstream &s, bool autoClose);
		};
	private:
		friend class WSHttpRunner;
		friend class RequestHandler;
		friend class HttpGetRequestHandler;
		friend class ResponseHandler;
		friend class CookieHandler;
		friend class HttpHeaderHandler;
	private:
		const string TAG = "ResponseCaller";
		int code;
		string msg;
		string query;
		unsigned long long bodyLen;
		HINTERNET request;
		map<string, string> responseHeaders;
		vector<Cookie> responseCookies;
	private:
		ResponseCaller() {}
		void setHandler(HINTERNET handler);
		void setCode(int _code);
		void setMsg(string _msg);
		void closeBody();
		void setBodySize(unsigned long long);
	public:
		ResponseCaller(int, string);
		~ResponseCaller();
		int getCode();
		unsigned long long getBodySize();
		string getMsg();
		BodyParser getBody();
		string toString();
		const map<string, string>& getResponseHeaders();
		const vector<Cookie>& getResponseCookies();
	};
};
