﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description: Cookie Entity. Parse the header string 
*|                [Set-Cookie] to Cookie [EXPORTED]                                                                                              
*|　Author: Jidcoo                                                
*|　Version: 1.0                                                 
*|　Date: 2021-09-16 18:40                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:                                                        
*|  Date:                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once

#include "../utils/Util.h"
using namespace std;
namespace WSHTTP {
	typedef time_t cdate;
	class WSHTTP_API Cookie {
	private:
		/** Cookie name */
		string name;
		/** Cookie value */
		string value;
		/** Domain attribute. */
		string  cookieDomain;
		/** Expiration {a time stamp ,type is time_t[long in wins]}. */
		cdate cookieExpiryDate;
		/** Path attribute. */
		string cookiePath;
		/** My secure flag. */
		bool secure;
		cdate creationDate;
		/** The {httpOnly} flag. Default is false */
		bool httpOnly;
	public:
		const static string PATH_ATTR;
		const static string DOMAIN_ATTR;
		const static string MAX_AGE_ATTR;
		const static string SECURE_ATTR;
		const static string EXPIRES_ATTR;
		const static string HTTP_ONLY_ATTR;
		const static string INVALID_COOKIE_HEAD;
	public:
		static string normalizePath(const WSHTTP::Cookie cookie);
		static Cookie parse(const string& header, const string&originDomain, const string&originPath);
	public:
		/**
		 * Default Constructor taking a name and a value. The value may be null.
		 * name The name.
		 * value The value.
		*/
		Cookie(const string& name, const string& val);

		~Cookie();
	public:
		string toString() const;

		bool isInvalid();
	public:
		/**
		 * Returns the name.
		 * return string name The name
		 */
		string getName();

		/**
		 * Returns the value.
		 * return string value The current value.
		 */
		string getValue();

		/**
		 * Returns the expiration {type is cdate} of the cookie, or 0
		 * if none exists.
		 * return Expiration {type is cdate} of the cookie, or 0
		 */
		cdate getExpiryDate();

		/**
		 * Returns false if the cookie should be discarded at the end
		 * of the "session"; true otherwise.
		 *
		 * return false if the cookie should be discarded at the end
		 *         of the "session"; true otherwise
		 */
		bool isPersistent();

		/**
		 * Returns domain attribute of the cookie. The value of the Domain
		 * attribute specifies the domain for which the cookie is valid.
		 *
		 * return the value of the domain attribute.
		 */
		string getDomain();

		/**
		 * Returns the path attribute of the cookie. The value of the Path
		 * attribute specifies the subset of URLs on the origin server to which
		 * this cookie applies.
		 *
		 * return The value of the path attribute.
		 */
		string getPath();

		/**
		 * Indicates whether this cookie requires a secure connection.
		 *
		 * return true if this cookie should only be sent
		 *          over secure connections, false otherwise.
		 */
		bool isSecure();

		/**
		 * Returns true if this cookie has expired.
		 * param date Current time
		 *
		 * return true if the cookie has expired.
		 */
		bool isExpired(const cdate& date) const;

		/**
		 * Returns creation time of the cookie.
		 */
		cdate getCreationDate();

		/**
		 * Checks whether this Cookie has been marked as httpOnly.
		 * <p>The default implementation returns false.
		 *
		 * return true if this Cookie has been marked as httpOnly,
		 * false otherwise
		 */
		bool isHttpOnly();
	public:

		void setValue(const string& value);

		/**
		 * Sets expiration date.
		 * param expiryDate the {type is cdate} after which this cookie is no longer valid.
		 */
		void setExpiryDate(const cdate& expiryDate);

		/**
		 * Sets the domain attribute.
		 * param domain The value of the domain attribute
		 */
		void setDomain(const string& domain);

		/**
		 * Sets the path attribute.
		 * param path The value of the path attribute
		 */
		void setPath(const string& path);

		/**
		 * Sets the secure attribute of the cookie.
		 * <p>
		 * When true the cookie should only be sent
		 * using a secure protocol (https).  This should only be set when
		 * the cookie's originating server used a secure protocol to set the
		 * cookie's value.
		 * param secure The value of the secure attribute
		 */
		void setSecure(bool secure);

		/**
		 * Marks or unmarks  this Cookie as httpOnly.
		 * param httpOnly true if this cookie is to be marked as
		 * httpOnly, false otherwise
		 *
		 */
		void setHttpOnly(bool httpOnly);
	};
};