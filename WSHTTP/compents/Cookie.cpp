﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description: Cookie Entity. Parse the header string
*|                [Set-Cookie] to Cookie [EXPORTED]
*|　Author: Jidcoo
*|　Version: 1.0
*|　Date: 2021-09-16 18:40
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/


#include "Cookie.h"
/* static member init */
const string WSHTTP::Cookie::PATH_ATTR = "path";
const string WSHTTP::Cookie::DOMAIN_ATTR = "domain";
const string WSHTTP::Cookie::MAX_AGE_ATTR = "max-age";
const string WSHTTP::Cookie::SECURE_ATTR = "secure";
const string WSHTTP::Cookie::EXPIRES_ATTR = "expires";
const string WSHTTP::Cookie::HTTP_ONLY_ATTR = "httponly";
const string WSHTTP::Cookie::INVALID_COOKIE_HEAD = "INVALID";
/* static member init */

string WSHTTP::Cookie::normalizePath(WSHTTP::Cookie cookie)
{
	string path(cookie.getPath());
	if (path.empty()) {
		path = "/";
	}
	if (path.size() - 1 > 0 && path[path.size() - 1] != '/') {
		path = path + '/';
	}
	return path;
}

WSHTTP::Cookie WSHTTP::Cookie::parse(const string & header, const string&originDomain, const string&originPath)
{
	vector<string> arr = Util::split(header, ";");
	vector<string> matching;
	string item;
	if (arr.size() > 0) {
		//the first item always as the [name,value] to matching
		item = arr[0];
		matching = Util::split(item, "=");
		if (matching.size()>=2) {
			matching[0] = item.substr(0, item.find_first_of('='));
			matching[1] = item.substr(item.find_first_of('=')+1);
			Cookie cookie(Util::trim(matching[0]), Util::trim(matching[1]));
			cookie.setDomain(originDomain);
			cookie.setPath(originPath.empty() ? "/" : originPath);
			//handle the left
			for (size_t i = 1; i < arr.size(); ++i) {
				item = arr[i];
				matching = Util::split(item, "=");
				if (matching.size() == 2) {
					string key = Util::trim(matching[0]);
					matching[1] = Util::trim(matching[1]);
					Util::toLowerString(key);
					if (key == PATH_ATTR) {
						cookie.setPath(!matching[1].empty() ? matching[1] : "/");
					}
					else if (key == DOMAIN_ATTR) {
						// Ignore domain attributes ending with '.' per RFC 6265, 4.1.2.3
						if (Util::endsWith(matching[1], ".")) {
							continue;
						}
						string domain = matching[1];
						if (Util::endsWith(domain, ".")) {
							domain = domain.substr(1);
						}
						Util::toLowerString(domain);
						cookie.setDomain(domain);
					}
					else if (key == MAX_AGE_ATTR) {
						int age = atoi(matching[1].c_str());
						if (age <=0) {
							cookie.setExpiryDate(0);
						}else {
							cookie.setExpiryDate(cookie.getCreationDate() + age);
						}
					}
					else if (key == EXPIRES_ATTR) {
						if (cookie.getExpiryDate() == 0) {
							cookie.setExpiryDate(Util::convertCookieDate(
								Util::trim(matching[1])));
						}
						else {}
					}
				}
				else {
					item = Util::trim(item);
					Util::toLowerString(item);
					if (item == HTTP_ONLY_ATTR) {
						cookie.setHttpOnly(true);
					}
					else if (item == SECURE_ATTR) {
						cookie.setSecure(true);
					}
				}
			}
			if (cookie.getDomain().empty()) { goto bad; }
			return cookie;
		}
	}
bad:return Cookie(INVALID_COOKIE_HEAD, INVALID_COOKIE_HEAD);//return if the header is invalid 
}

WSHTTP::Cookie::Cookie(const string & name, const string & val)
{
	if (name.empty()) {
		this->name = INVALID_COOKIE_HEAD;
		this->value = INVALID_COOKIE_HEAD;
	}
	else {
		this->name = name;
		this->value = val;
	}
	time(&this->creationDate);
	this->httpOnly = false;
	this->cookieExpiryDate = 0;
	this->secure = false;
}

WSHTTP::Cookie::~Cookie() {}

string WSHTTP::Cookie::toString()
const {
	string buffer;
	buffer.append("[name: ");
	buffer.append(this->name);
	buffer.append("; ");
	buffer.append("value: ");
	buffer.append(this->value);
	buffer.append("; ");
	buffer.append("domain: ");
	buffer.append(this->cookieDomain);
	buffer.append("; ");
	buffer.append("path: ");
	buffer.append(this->cookiePath);
	buffer.append("; ");
	buffer.append("expiry: ");
	buffer.append(Util::convertTime_T(this->cookieExpiryDate == -1 ? -1 : (this->cookieExpiryDate + 28800)));
	//buffer.append("expiry: ");
	//buffer.append(to_string(this->cookieExpiryDate));
	buffer.append("; ");
	buffer.append("secure: ");
	buffer.append(to_string(this->secure));
	buffer.append("; ");
	buffer.append("creationDate: ");
	buffer.append(Util::convertTime_T(this->creationDate));
	buffer.append("; ");
	buffer.append("httpOnly: ");
	buffer.append(to_string(this->httpOnly));
	buffer.append("]");
	return buffer;
}

bool WSHTTP::Cookie::isInvalid()
{
	return this->name == INVALID_COOKIE_HEAD;
}

string WSHTTP::Cookie::getName()
{
	return this->name;
}

string WSHTTP::Cookie::getValue()
{
	return this->value;
}

WSHTTP::cdate WSHTTP::Cookie::getExpiryDate()
{
	return this->cookieExpiryDate;
}

bool WSHTTP::Cookie::isPersistent()
{
	return cookieExpiryDate != 0;
}

string WSHTTP::Cookie::getDomain()
{
	return this->cookieDomain;
}

string WSHTTP::Cookie::getPath()
{
	return this->cookiePath;
}

bool WSHTTP::Cookie::isSecure()
{
	return this->secure;
}

bool WSHTTP::Cookie::isExpired(const cdate & date)const {
	if (this->cookieExpiryDate == -1)return false;
	return this->cookieExpiryDate - date <= 0;
}

WSHTTP::cdate WSHTTP::Cookie::getCreationDate()
{
	return creationDate;
}

bool WSHTTP::Cookie::isHttpOnly()
{
	return this->httpOnly;
}

void WSHTTP::Cookie::setValue(const string & value)
{
	this->value = value;
}

void WSHTTP::Cookie::setExpiryDate(const cdate & expiryDate)
{
	this->cookieExpiryDate = expiryDate;
}

void WSHTTP::Cookie::setDomain(const string & domain)
{
	cookieDomain = domain;
	if (!domain.empty()) {
		Util::toLowerString(cookieDomain);
	}
}

void WSHTTP::Cookie::setPath(const string & path)
{
	this->cookiePath = path;
}

void WSHTTP::Cookie::setSecure(bool secure)
{
	this->secure = secure;
}

void WSHTTP::Cookie::setHttpOnly(bool httpOnly)
{
	this->httpOnly = httpOnly;
}



