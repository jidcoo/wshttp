﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:HttpConnection [EXPORTED]
*|　Author:Jidcoo
*|　Version:1.1
*|　Date:2022-06-28 20:00
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:新增了setSendTimeoutWithHandle、setSendTimeoutWithHandle、setResponseTimeoutWithHandle
*|       setTimeoutConfigWithHandle方法
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpConnection.h"

WSHTTP::HttpConnection::HttpConnection(string _url)
	:HttpConnection(_url, Config::getDefaultConfig())
{

}

WSHTTP::HttpConnection::HttpConnection(string _url, Config _config)
{
	__curl = nullptr;
	url = _url;
	config = _config;
	isOpened = false;
}

WSHTTP::HttpConnection::~HttpConnection()
{
	close();
	if (__curl != nullptr){
		delete __curl;
	}
}


bool WSHTTP::HttpConnection::open()
{
	if(__curl==nullptr){
		__curl = CURL::parse(url);
	}
	if (__curl->isVaild()) {
		if (!isClosed())
			return true;
		string lpszAgent;
		DWORD dwAccessType;
		string lpszProxy;
		string lpszProxyBypass;
		DWORD dwFlags;
		parseConfigOnOpen(lpszAgent, dwAccessType, lpszProxy, lpszProxyBypass, dwFlags);
		mSession = InternetOpenA(lpszAgent.c_str(), dwAccessType, lpszProxy.empty() ? NULL : lpszProxy.c_str(), lpszProxyBypass.empty() ? NULL : lpszProxyBypass.c_str(), dwFlags);
		if (mSession == NULL) {
			DWORD error_code = GetLastError();
			Util::LOG(L_ERROR, "HttpConnection", "ERROR_CODE=" + to_string(error_code));
			return false;
		}
		setTimeoutConfigWithHandle(mSession);
		string userName;
		string pwd;
		DWORD dwService;
		DWORD dwFlags1;
		parseConfigOnConnect(userName, pwd, dwService, dwFlags1);
		/**
		 * 如果是Https协议：
		   如果有端口号，使用指定端口号，没有就使用默认的443
		   如果是Http协议
		   如果有端口号，使用指定端口号，没有就使用默认的80
		 */
		INTERNET_PORT PORT;
		string HOST_NAME;
		string HTTPS_TAG("HTTPS");
		string HTTP_TAG("HTTP");
		if (__curl->getUpperCastScheme() == HTTPS_TAG) {//HTTPS协议
			if (__curl->getPort() == -1) {
				PORT = INTERNET_DEFAULT_HTTPS_PORT;
			}else {
				PORT = __curl->getPort();
			}
			HOST_NAME = __curl->getHostName();
		}else if (__curl->getUpperCastScheme() == HTTP_TAG) {
			if (__curl->getPort() == -1) {
				PORT = INTERNET_DEFAULT_HTTP_PORT;
			}else {
				PORT = __curl->getPort();
			}
			HOST_NAME = __curl->getHostName();
		}else {//协议无效
			Util::LOG(L_ERROR, "HttpConnection", "Scheme is invalid:" + __curl->getUpperCastScheme());
			releaseHandle(mSession);
			return false;
		}
		mConnect = InternetConnectA(mSession, HOST_NAME.c_str(), PORT, userName.empty() ? NULL : userName.c_str(), pwd.empty() ? NULL : pwd.c_str(), dwService, dwFlags1, NULL);
		if (NULL == mConnect) {
			string error_code = to_string(GetLastError());
			Util::LOG(L_ERROR, "HttpConnection", "Connection failed，errCode=" + (error_code));
			releaseHandle(mSession);
			return false;
		}
		setTimeoutConfigWithHandle(mConnect);
		Util::LOG(L_INFO, "HttpConnection", "Connection open successfully");
		isOpened = true;
		return isOpened;
	}else {
		Util::LOG(L_ERROR, "HttpConnection", "URL is invalid:" + url);
		return false;
	}
}

void WSHTTP::HttpConnection::close()
{
	if (!isClosed()) {
		releaseHandle(mConnect);
		releaseHandle(mSession);
		isOpened = false;
		Util::LOG(L_INFO, "HttpConnection", "Connection close successfully");
	}
}

bool WSHTTP::HttpConnection::isClosed()
{
	return !isOpened;
}

WSHTTP::Config & WSHTTP::HttpConnection::getConnectConfig()
{
	return this->config;
}

void WSHTTP::HttpConnection::parseConfigOnOpen(string& lpszAgent, DWORD &dwAccessType, string& lpszProxy, string& lpszProxyBypass, DWORD &dwFlags)
{
	if(config.getUserAgent().empty()) {
		lpszAgent = "WSHTTP V1.0 Release";
	}
	else {
		lpszAgent = config.getUserAgent();
	}
	switch (config.getAccessType()) {
	case Config::DIRECT:
		dwAccessType = INTERNET_OPEN_TYPE_DIRECT;
		lpszProxy = "";
		break;
	case Config::PRECONFIG:
		dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG;
		lpszProxy = "";
		break;
	case Config::PROXY:
		dwAccessType = INTERNET_OPEN_TYPE_PROXY;
		lpszProxy =config.getHttpProxy().toString();
		if(lpszProxy.empty()){
			dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG;
		}
		break;
	default:
		dwAccessType = INTERNET_OPEN_TYPE_PRECONFIG;
		lpszProxy = "";
		break;
	}
	lpszProxyBypass = "";
	/*if (config.getProxyBypass().empty()) {
		lpszProxyBypass = "";
	}
	else {
		lpszProxyBypass = config.getProxyBypass();
	}*/
	//desc proxyBypass
	/**
	 * Pointer to a null-terminated string that specifies an optional list of
	 host names or IP addresses, or both, that should not be routed through
	 the proxy when dwAccessType is set to INTERNET_OPEN_TYPE_PROXY. 
	 The list can contain wildcards. Do not use an empty string, because
	 InternetOpen will use it as the proxy bypass list. If this 
	 parameter specifies the "<local>" macro, the function bypasses 
     the proxy for any host name that does not contain a period.
     By default, WinINet will bypass the proxy for requests that use the host 
	 names "localhost", "loopback", "127.0.0.1", or "[::1]". This behavior 
	 exists because a remote proxy server typically will not resolve these 
	 addresses properly.Internet Explorer 9:  You can remove the
	 local computer from the proxy bypass list using the "<-loopback>" macro.
     If dwAccessType is not set to INTERNET_OPEN_TYPE_PROXY, this
	 parameter is ignored and should be NULL.
	 */
	switch (config.getFlag()){
	case Config::F_NULL:
		dwFlags = NULL;
		break;
	case Config::ASYNC:
		dwFlags = INTERNET_FLAG_ASYNC;
		break;
	case Config::FROM_CACHE:
		dwFlags = INTERNET_FLAG_FROM_CACHE;
		break;
	default:
		dwFlags = NULL;
		break;
	}
}

void WSHTTP::HttpConnection::parseConfigOnConnect(string& lpszUserName, string& lpszPassword, DWORD& dwService, DWORD& dwFlags)
{
	lpszUserName = "";
	lpszPassword = "";
	dwService = INTERNET_SERVICE_HTTP;
	dwFlags = NULL;
}

bool WSHTTP::HttpConnection::setConnectTimeoutWithHandle(HINTERNET &hInternet, DWORD & timeout)
{
	if (hInternet) {
		return InternetSetOption(hInternet, INTERNET_OPTION_CONNECT_TIMEOUT, (void *)&timeout,sizeof(DWORD));
	}
	return false;
}

bool WSHTTP::HttpConnection::setSendTimeoutWithHandle(HINTERNET &hInternet, DWORD & timeout)
{
	if (hInternet) {
		return InternetSetOption(hInternet, INTERNET_OPTION_SEND_TIMEOUT, (void *)&timeout, sizeof(DWORD));
	}
	return false;
}

bool WSHTTP::HttpConnection::setResponseTimeoutWithHandle(HINTERNET &hInternet, DWORD & timeout)
{
	if (hInternet) {
		return InternetSetOption(hInternet, INTERNET_OPTION_RECEIVE_TIMEOUT, (void *)&timeout, sizeof(DWORD));
	}
	return false;
}

void WSHTTP::HttpConnection::setTimeoutConfigWithHandle(HINTERNET &hInternet)
{
	if (!hInternet) return;
	DWORD connectionTimeout = config.getConnectTimeout() == -1 ? 0xFFFFFFFF : config.getConnectTimeout()*1000;
	if(!setConnectTimeoutWithHandle(hInternet,connectionTimeout)){
		Util::LOG(L_INFO, "HttpConnection", "SetConnectTimeout failed.Err code="+to_string(GetLastError()));
	}
	DWORD sendTimeout = config.getSendTimeout() == -1 ? -1: config.getSendTimeout() * 1000;
	if (!setSendTimeoutWithHandle(hInternet, sendTimeout)) {
		Util::LOG(L_INFO, "HttpConnection", "SetSendTimeout failed.Err code=" + to_string(GetLastError()));
	}
	DWORD responseTimeout = config.getResponseTimeout() == -1 ? -1 : config.getResponseTimeout() * 1000;
	if (!setResponseTimeoutWithHandle(hInternet, responseTimeout)) {
		Util::LOG(L_INFO, "HttpConnection", "SetResponseTimeout failed.Err code=" + to_string(GetLastError()));
	}
}

void WSHTTP::HttpConnection::releaseHandle(HINTERNET &hInternet)
{
	if (hInternet) {
		InternetCloseHandle(hInternet);
		hInternet = NULL;
	}
}
