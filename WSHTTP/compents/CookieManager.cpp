﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description: CookieManager Manage Cookie From Connection
*|　             ---Thread Safe Class [UN-EXPORTED]
*|　Author: Jidcoo
*|　Version: 1.0
*|　Date: 2021-09-16 18:50
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "CookieManager.h"

bool WSHTTP::CookieManager::CookieIdentityComparator::operator () 
(const Cookie& c1_1, const Cookie& c2_1) const {
	Cookie c1(c1_1);
	Cookie c2(c2_1);
	int res = c1.getName().compare(c2.getName());
	if (res == 0) {
		// do not differentiate empty and null domains
		string d1 = c1.getDomain();
		if (d1.empty()) {
			d1 = "";
		}
		string d2 = c2.getDomain();
		if (d2.empty()) {
			d2 = "";
		}
		Util::toLowerString(d1);
		Util::toLowerString(d2);
		res = d1.compare(d2);
	}
	if (res == 0) {
		string p1 = c1.getPath();
		if (p1.empty()) {
			p1 = "/";
		}
		string p2 = c2.getPath();
		if (p2.empty()) {
			p2 = "/";
		}
		res = p1.compare(p2);
	}
	return res > 0;
}


WSHTTP::CookieManager::CookieManager()
{
	//init readwrite lock
	lock.initLock();
}

WSHTTP::CookieManager::~CookieManager()
{
	cookies.clear();
	Util::LOG(L_INFO, TAG, "CookieManager destroyed");
}

void WSHTTP::CookieManager::addCookie(Cookie& cookie)
{
	if (cookie.getName() != Cookie::INVALID_COOKIE_HEAD) {
		lock.writeLock();
		// first remove any old cookie that is equivalent
		cookies.erase(cookie);
		if (!cookie.isExpired(Util::nowTime())) {
			cookies.insert(cookie);
		}
		lock.writeUnlock();
	}
}

vector<WSHTTP::Cookie> WSHTTP::CookieManager::getCookies()
{
	lock.readLock();
	vector<Cookie> vc(cookies.begin(), cookies.end());
	lock.readUnlock();
	return vc;
}

bool WSHTTP::CookieManager::clearExpired(const cdate& date)
{
	if (date <= 0) {
		return false;
	}
	lock.writeLock();
	bool removed = false;
	for (set<Cookie>::iterator it = cookies.begin(); it != cookies.end(); ) {
		if (it->isExpired(date)) {
			it = cookies.erase(it);
			removed = true;
		}
		else {
			it++;
		}
	}
	lock.writeUnlock();
	return removed;
}

void WSHTTP::CookieManager::clear()
{
	lock.writeLock();
	cookies.clear();
	lock.writeUnlock();
}

string WSHTTP::CookieManager::toString()
{
	string str;
	lock.readLock();
	for (set<Cookie>::iterator it = cookies.begin(); it != cookies.end(); ) {
		str.append(it->toString());
		str.append("\n");
		it++;
	}
	lock.readUnlock();
	return str;
}


bool WSHTTP::CookieManager::domainMatch(string & domain, string & host)
{
	if (Util::isIPv4Address(host) || Util::isIPv6Address(host)) {
		return false;
	}
	string normalizedDomain = Util::startsWith(domain, ".") ? domain.substr(1) : domain;
	if (Util::endsWith(host, normalizedDomain)) {
		const int prefix = host.length() - normalizedDomain.length();
		// Either a full match or a prefix ending with a '.'
		if (prefix == 0) {
			return true;
		}
		return prefix > 1 && host[prefix - 1] == '.';
	}
	return false;
}

bool WSHTTP::CookieManager::matchCookie(Cookie & cookie, const string & originHost, const int & port, const string & uriPath_, const bool & secure)
{
	string uriPath(uriPath_);
	if(uriPath.empty()){
		uriPath = "/";
	}
	/* match path */
	string normalizedCookiePath = cookie.getPath();
	if (normalizedCookiePath.empty()) {
		normalizedCookiePath = "/";
	}
	if (normalizedCookiePath.length() > 1 && Util::endsWith(normalizedCookiePath, "/")) {
		normalizedCookiePath = normalizedCookiePath.substr(0, normalizedCookiePath.length() - 1);
	}
	if (Util::startsWith(uriPath, normalizedCookiePath)) {
		if (normalizedCookiePath.compare(string("/")) == 0) {
			goto matchingSuffixDomain;
		}
		if (uriPath.length() == normalizedCookiePath.length()) {
			goto matchingSuffixDomain;
		}
		if (uriPath[normalizedCookiePath.length()] == '/') {
			goto matchingSuffixDomain;
		}
	}
	return false;
	/* match path */
matchingSuffixDomain:
	/* match suffixDomain */
	if (cookie.getDomain().empty()) {
		return false;
	}
	// Validate the cookies domain attribute.  NOTE:  Domains without
	// any dots are allowed to support hosts on private LANs that don't
	// have DNS names.  Since they have no dots, to domain-match the
	// request-host and domain must be identical for the cookie to sent
	// back to the origin-server.
	string host = originHost;
	string domain = cookie.getDomain();
	if (Util::startsWith(domain, ".")) {
		domain = domain.substr(1);
	}
	Util::toLowerString(domain);
	Util::toLowerString(host);
	string normalizedDomain;
	if (host.compare(domain) == 0) {
		goto matchMaxAge;
	}
	if (Util::isIPv4Address(host) || Util::isIPv6Address(host)) {
		return false;
	}
	normalizedDomain = Util::startsWith(domain, ".") ? domain.substr(1) : domain;
	if (Util::endsWith(host, normalizedDomain)) {
		const int prefix = host.length() - normalizedDomain.length();
		// Either a full match or a prefix ending with a '.'
		if (prefix == 0) {
			goto matchMaxAge;
		}
		if (prefix > 1 && host[prefix - 1] == '.') {
			goto matchMaxAge;
		}
	}
	return false;
	/* match suffixDomain */
matchMaxAge:
	/* match MaxAge */
	// Always match maxAge to true
	goto matchSecure;
	/* match MaxAge */
matchSecure:
	/* match Secure */
	if (!cookie.isSecure() || secure) {
		goto matchHttpOnly;
	}
	return false;
	/* match Secure */
matchHttpOnly:
	/* match HttpOnly */
	// Always match HttpOnly to true
	goto matchExpires;
	/* match HttpOnly */
matchExpires:
	/* match Expires */
	// Always match Expires to true
	return true;
	/* match Expires */
}

bool WSHTTP::CookieManager::validateCookie(Cookie & cookie, const string & _host, const int & port, const string & path, const bool & secure)
{
	const string TAG = "CookieManager";
	//checkPath ignore
	//checkDomain
	string host = _host;
	string domain = cookie.getDomain();
	if (domain.empty()) {
		Util::LOG(L_WARNNING, TAG, "ValidateCookie:Bad cookie:empty domain");
		return false;
	}
	if (!(host.compare(domain) == 0) && !domainMatch(domain, host)) {
		string msg = "Illegal 'domain' attribute \"";
		msg += domain;
		msg += "\". Domain of origin: \"";
		msg += host;
		msg += "\"";
		//Util::LOG(L_WARNNING,TAG,msg);
		return false;
	}
	//checkMaxAge ignore
	//checkSecure ignore
	//checkHttpOnly ignore
	//checkExpires ignore
	return true;
}




