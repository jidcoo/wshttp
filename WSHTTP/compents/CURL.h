﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:CURL class;Parse the string to URL[URL Class][UN-EXPORTED]                                          
*|　Author:Jidcoo                                                
*|　Version:1.0                                                 
*|　Date:2021-09-16 13:03                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:                                                        
*|  Date:                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once

#include "../utils/Util.h"
using namespace std;
namespace WSHTTP {
	class  CURL {
	private:
		bool vaild;
		string scheme;
		string username;
		string password;
		string host;
		string hostname;
		int port;
		string path;
		string rawPath;
		string query;
		string fragment;
		string opaque;
	private:
		CURL(string url);
		bool nullOrEmpty(string url);
		static bool isPortValid(string portStr);
		static CURL* parseA(CURL* url_,string url);
	public:
		CURL();
		bool isVaild();
		void isVaild(bool);
		bool isSecure();
		string getScheme();
		string getUpperCastScheme();
		void setScheme(string);
		string getUserName();
		void setUserName(string);
		string getPassword();
		void setPassword(string);
		string getHost();
		void setHost(string);
		string getHostName();
		void setHostName(string);
		int getPort();
		void setPort(int);
		string getPath();
		void setPath(string);
		string getRawPath();
		void setRawPath(string);
		string getQuery();
		void setQuery(string);
		string getFragment();
		void setFragment(string);
		string getOpaque();
		void setOpaque(string);
		void parseScheme(string &remaining, string &scheme);
		void parseUserInfo(string &authority, string &user, string &password);
		void parseHost(string &authority, string &host);
		string toString() const;
		static CURL* parse(string url);
		static CURL  _parse_(string url);
	};
};
