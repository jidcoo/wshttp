﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description: CookieManager Manage Cookie From Connection                                              
*|　             ---Thread Safe Class [UN-EXPORTED]
*|　Author: Jidcoo                                                
*|　Version: 1.0                                                 
*|　Date: 2021-09-16 18:50                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:                                                        
*|  Date:                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "Cookie.h"
#include "../utils/Util.h"
#include "../utils/WinSRWLock.h"
using namespace std;
namespace WSHTTP {
	class CookieManager{
	private:
		class CookieIdentityComparator {
		public:
			bool operator () (const Cookie& c1_1, const Cookie& c2_1)const;
		};
	private:
		set<Cookie, CookieIdentityComparator> cookies;
		WinSRWLock lock;
		const string TAG = "CookieManager";
	private:

		static bool domainMatch(string &domain, string &host);

		/**
		 * Determines if a Cookie matches the target location.
		 *
		 * param cookie the Cookie to be matched
		 * other params the target to test against
		 *
		 * return true if the cookie should be submitted with a request
		 *  with given attributes, false otherwise.
		 */
		static bool matchCookie(Cookie& cookie, const string& host, const int& port, const string& path, const bool& secure);
		static bool validateCookie(Cookie& cookie, const string& host, const int& port, const string& path, const bool& secure);
	public:
		CookieManager();
		~CookieManager();
	public:
		friend class CookieHandler;
		friend class ResponseHandler;
	public:
		/**
		 * Adds an Cookie, replacing any existing equivalent cookies.
		 * If the given cookie has already expired it will not be added, but existing
		 * values will still be removed.
		 * param cookie the Cookie to be added
		*/
		void addCookie(Cookie& cookie);

		/**
		 * Returns all cookies contained in this manager.
		 * return all cookies
		 */
		vector<Cookie> getCookies();

		/**
		 * Removes all of Cookies in this manager that have expired by
		 * the specified cdate.
		 * return true if any cookies were purged.
		 */
		bool clearExpired(const cdate& date);

		/**
		 * Clears all cookies.
		 */
		void clear();

		/**
		 * To printable
		 */
		string toString();
	};

};