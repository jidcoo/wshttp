﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:HttpConnection [EXPORTED]                                              
*|　Author:Jidcoo                                                
*|　Version:1.1                                                 
*|　Date:2022-06-28 20:00                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:新增了setSendTimeoutWithHandle、setSendTimeoutWithHandle、setResponseTimeoutWithHandle
*|       setTimeoutConfigWithHandle方法
*|  Date:                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "../framework.h"
#include "../config/Config.h"
#include "../compents/CURL.h"
using namespace std;
namespace WSHTTP{
	class WSHTTP_API HttpConnection {
   	public:
		HttpConnection(string url);
		HttpConnection(string, Config);
		~HttpConnection();
		bool open();
		void close();
		bool isClosed();
		Config& getConnectConfig();
	private:
		void parseConfigOnOpen(string&, DWORD&, string&, string&, DWORD&);//解析配置
		void parseConfigOnConnect(string&, string&, DWORD&, DWORD&); //解析配置
		bool setConnectTimeoutWithHandle(HINTERNET&,DWORD& timeout);//为任何网络句柄设置连接超时时间
		bool setSendTimeoutWithHandle(HINTERNET&, DWORD& timeout);//为任何网络句柄设置发送超时时间
		bool setResponseTimeoutWithHandle(HINTERNET&, DWORD& timeout);//为任何网络句柄设置响应超时时间
		void setTimeoutConfigWithHandle(HINTERNET&);
		void releaseHandle(HINTERNET&);//释放句柄
	private:
		string url;
		CURL* __curl;
		Config config;
	private:
		HINTERNET mSession;
		HINTERNET mConnect;
		bool isOpened;
	private:
		friend class RequestHandler;
		friend class HttpGetRequestHandler;
		friend class ResponseHandler;
	};
};