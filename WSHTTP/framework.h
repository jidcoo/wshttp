﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:WSHTTP framework header files
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2021-04-15 09:43
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:Add Header file "Util.h"
*|  Date:2021-04-18 16:14
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#define WIN32_LEAN_AND_MEAN
#ifndef HWSHTTP
#define HWSHTTP
#include <windows.h>
#include <wininet.h>
#include "utils/Util.h"
#pragma comment(lib, "Wininet.lib")
#pragma warning(disable:4251)
#ifdef WSHTTP_EXPORTS
#define WSHTTP_API __declspec(dllexport)
#else
#define WSHTTP_API __declspec(dllimport)
#endif
#endif // !HWSHTTP