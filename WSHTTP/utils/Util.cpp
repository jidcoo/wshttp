﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:WSHTTP Utils Set [Static Class][UN-EXPORTED]
*|　Author:Jidcoo
*|　Version:1.1
*|　Date:2022-06-27 11:16
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:trim()方法重构功能为删除字符串开头和结尾的不可见字符
*|  Date:2022-06-27 11:16
*-------------------------------------------------------------------------------------------------------------
*/

#include "Util.h"

/*static member init*/
bool WSHTTP::Util::openLog = true;
map<string, string> WSHTTP::Util::mimeTypeMap = WSHTTP::Util::initFileMimeTypeMap();

map<string, string> WSHTTP::Util::initFileMimeTypeMap(){
	return map<string, string>();
}
/*static member init*/

/**
 * private constructor
 */
WSHTTP::Util::Util() {}

WSHTTP::Util::~Util() {}



void WSHTTP::Util::fillFileMimeTypeMap()
{
	if (mimeTypeMap.size() == 0) {
		mimeTypeMap.insert(pair<string, string>("", "application/octet-stream"));
		mimeTypeMap.insert(pair<string, string>(".tif", "image/tiff"));
		mimeTypeMap.insert(pair<string, string>(".001", "application/x-001"));
		mimeTypeMap.insert(pair<string, string>(".301", "application/x-301"));
		mimeTypeMap.insert(pair<string, string>(".323", "text/h323"));
		mimeTypeMap.insert(pair<string, string>(".906", "application/x-906"));
		mimeTypeMap.insert(pair<string, string>(".907", "drawing/907"));
		mimeTypeMap.insert(pair<string, string>(".a11", "application/x-a11"));
		mimeTypeMap.insert(pair<string, string>(".acp", "audio/x-mei-aac"));
		mimeTypeMap.insert(pair<string, string>(".ai", "application/postscript"));
		mimeTypeMap.insert(pair<string, string>(".aif", "audio/aiff"));
		mimeTypeMap.insert(pair<string, string>(".aifc", "audio/aiff"));
		mimeTypeMap.insert(pair<string, string>(".aiff", "audio/aiff"));
		mimeTypeMap.insert(pair<string, string>(".anv", "application/x-anv"));
		mimeTypeMap.insert(pair<string, string>(".asa", "text/asa"));
		mimeTypeMap.insert(pair<string, string>(".asf", "video/x-ms-asf"));
		mimeTypeMap.insert(pair<string, string>(".asp", "text/asp"));
		mimeTypeMap.insert(pair<string, string>(".asx", "video/x-ms-asf"));
		mimeTypeMap.insert(pair<string, string>(".au", "audio/basic"));
		mimeTypeMap.insert(pair<string, string>(".avi", "video/avi"));
		mimeTypeMap.insert(pair<string, string>(".awf", "application/vnd.adobe.workflow"));
		mimeTypeMap.insert(pair<string, string>(".biz", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".bmp", "application/x-bmp"));
		mimeTypeMap.insert(pair<string, string>(".bot", "application/x-bot"));
		mimeTypeMap.insert(pair<string, string>(".c4t", "application/x-c4t"));
		mimeTypeMap.insert(pair<string, string>(".c90", "application/x-c90"));
		mimeTypeMap.insert(pair<string, string>(".cal", "application/x-cals"));
		mimeTypeMap.insert(pair<string, string>(".cat", "application/vnd.ms-pki.seccat"));
		mimeTypeMap.insert(pair<string, string>(".cdf", "application/x-netcdf"));
		mimeTypeMap.insert(pair<string, string>(".cdr", "application/x-cdr"));
		mimeTypeMap.insert(pair<string, string>(".cel", "application/x-cel"));
		mimeTypeMap.insert(pair<string, string>(".cer", "application/x-x509-ca-cert"));
		mimeTypeMap.insert(pair<string, string>(".cg4", "application/x-g4"));
		mimeTypeMap.insert(pair<string, string>(".cgm", "application/x-cgm"));
		mimeTypeMap.insert(pair<string, string>(".cit", "application/x-cit"));
		mimeTypeMap.insert(pair<string, string>(".class", "java/*"));
		mimeTypeMap.insert(pair<string, string>(".cml", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".cmp", "application/x-cmp"));
		mimeTypeMap.insert(pair<string, string>(".cmx", "application/x-cmx"));
		mimeTypeMap.insert(pair<string, string>(".cot", "application/x-cot"));
		mimeTypeMap.insert(pair<string, string>(".crl", "application/pkix-crl"));
		mimeTypeMap.insert(pair<string, string>(".crt", "application/x-x509-ca-cert"));
		mimeTypeMap.insert(pair<string, string>(".csi", "application/x-csi"));
		mimeTypeMap.insert(pair<string, string>(".css", "text/css"));
		mimeTypeMap.insert(pair<string, string>(".cut", "application/x-cut"));
		mimeTypeMap.insert(pair<string, string>(".dbf", "application/x-dbf"));
		mimeTypeMap.insert(pair<string, string>(".dbm", "application/x-dbm"));
		mimeTypeMap.insert(pair<string, string>(".dbx", "application/x-dbx"));
		mimeTypeMap.insert(pair<string, string>(".dcd", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".dcx", "application/x-dcx"));
		mimeTypeMap.insert(pair<string, string>(".der", "application/x-x509-ca-cert"));
		mimeTypeMap.insert(pair<string, string>(".dgn", "application/x-dgn"));
		mimeTypeMap.insert(pair<string, string>(".dib", "application/x-dib"));
		mimeTypeMap.insert(pair<string, string>(".dll", "application/x-msdownload"));
		mimeTypeMap.insert(pair<string, string>(".doc", "application/msword"));
		mimeTypeMap.insert(pair<string, string>(".dot", "application/msword"));
		mimeTypeMap.insert(pair<string, string>(".drw", "application/x-drw"));
		mimeTypeMap.insert(pair<string, string>(".dtd", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".dwf", "Model/vnd.dwf"));
		mimeTypeMap.insert(pair<string, string>(".dwf", "application/x-dwf"));
		mimeTypeMap.insert(pair<string, string>(".dwg", "application/x-dwg"));
		mimeTypeMap.insert(pair<string, string>(".dxb", "application/x-dxb"));
		mimeTypeMap.insert(pair<string, string>(".dxf", "application/x-dxf"));
		mimeTypeMap.insert(pair<string, string>(".edn", "application/vnd.adobe.edn"));
		mimeTypeMap.insert(pair<string, string>(".emf", "application/x-emf"));
		mimeTypeMap.insert(pair<string, string>(".eml", "message/rfc822"));
		mimeTypeMap.insert(pair<string, string>(".ent", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".epi", "application/x-epi"));
		mimeTypeMap.insert(pair<string, string>(".eps", "application/x-ps"));
		mimeTypeMap.insert(pair<string, string>(".eps", "application/postscript"));
		mimeTypeMap.insert(pair<string, string>(".etd", "application/x-ebx"));
		mimeTypeMap.insert(pair<string, string>(".exe", "application/x-msdownload"));
		mimeTypeMap.insert(pair<string, string>(".fax", "image/fax"));
		mimeTypeMap.insert(pair<string, string>(".fdf", "application/vnd.fdf"));
		mimeTypeMap.insert(pair<string, string>(".fif", "application/fractals"));
		mimeTypeMap.insert(pair<string, string>(".fo", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".frm", "application/x-frm"));
		mimeTypeMap.insert(pair<string, string>(".g4", "application/x-g4"));
		mimeTypeMap.insert(pair<string, string>(".gbr", "application/x-gbr"));
		mimeTypeMap.insert(pair<string, string>(".", "application/x-"));
		mimeTypeMap.insert(pair<string, string>(".gif", "image/gif"));
		mimeTypeMap.insert(pair<string, string>(".gl2", "application/x-gl2"));
		mimeTypeMap.insert(pair<string, string>(".gp4", "application/x-gp4"));
		mimeTypeMap.insert(pair<string, string>(".hgl", "application/x-hgl"));
		mimeTypeMap.insert(pair<string, string>(".hmr", "application/x-hmr"));
		mimeTypeMap.insert(pair<string, string>(".hpg", "application/x-hpgl"));
		mimeTypeMap.insert(pair<string, string>(".hpl", "application/x-hpl"));
		mimeTypeMap.insert(pair<string, string>(".hqx", "application/mac-binhex40"));
		mimeTypeMap.insert(pair<string, string>(".hrf", "application/x-hrf"));
		mimeTypeMap.insert(pair<string, string>(".hta", "application/hta"));
		mimeTypeMap.insert(pair<string, string>(".htc", "text/x-component"));
		mimeTypeMap.insert(pair<string, string>(".htm", "text/html"));
		mimeTypeMap.insert(pair<string, string>(".html", "text/html"));
		mimeTypeMap.insert(pair<string, string>(".htt", "text/webviewhtml"));
		mimeTypeMap.insert(pair<string, string>(".htx", "text/html"));
		mimeTypeMap.insert(pair<string, string>(".icb", "application/x-icb"));
		mimeTypeMap.insert(pair<string, string>(".ico", "image/x-icon"));
		mimeTypeMap.insert(pair<string, string>(".ico", "application/x-ico"));
		mimeTypeMap.insert(pair<string, string>(".iff", "application/x-iff"));
		mimeTypeMap.insert(pair<string, string>(".ig4", "application/x-g4"));
		mimeTypeMap.insert(pair<string, string>(".igs", "application/x-igs"));
		mimeTypeMap.insert(pair<string, string>(".iii", "application/x-iphone"));
		mimeTypeMap.insert(pair<string, string>(".img", "application/x-img"));
		mimeTypeMap.insert(pair<string, string>(".ins", "application/x-internet-signup"));
		mimeTypeMap.insert(pair<string, string>(".isp", "application/x-internet-signup"));
		mimeTypeMap.insert(pair<string, string>(".IVF", "video/x-ivf"));
		mimeTypeMap.insert(pair<string, string>(".java", "java/*"));
		mimeTypeMap.insert(pair<string, string>(".jfif", "image/jpeg"));
		mimeTypeMap.insert(pair<string, string>(".jpe", "image/jpeg"));
		mimeTypeMap.insert(pair<string, string>(".jpe", "application/x-jpe"));
		mimeTypeMap.insert(pair<string, string>(".jpeg", "image/jpeg"));
		mimeTypeMap.insert(pair<string, string>(".jpg", "image/jpeg"));
		mimeTypeMap.insert(pair<string, string>(".jpg", "application/x-jpg"));
		mimeTypeMap.insert(pair<string, string>(".js", "application/x-javascript"));
		mimeTypeMap.insert(pair<string, string>(".jsp", "text/html"));
		mimeTypeMap.insert(pair<string, string>(".la1", "audio/x-liquid-file"));
		mimeTypeMap.insert(pair<string, string>(".lar", "application/x-laplayer-reg"));
		mimeTypeMap.insert(pair<string, string>(".latex", "application/x-latex"));
		mimeTypeMap.insert(pair<string, string>(".lavs", "audio/x-liquid-secure"));
		mimeTypeMap.insert(pair<string, string>(".lbm", "application/x-lbm"));
		mimeTypeMap.insert(pair<string, string>(".lmsff", "audio/x-la-lms"));
		mimeTypeMap.insert(pair<string, string>(".ls", "application/x-javascript"));
		mimeTypeMap.insert(pair<string, string>(".ltr", "application/x-ltr"));
		mimeTypeMap.insert(pair<string, string>(".m1v", "video/x-mpeg"));
		mimeTypeMap.insert(pair<string, string>(".m2v", "video/x-mpeg"));
		mimeTypeMap.insert(pair<string, string>(".m3u", "audio/mpegurl"));
		mimeTypeMap.insert(pair<string, string>(".m4e", "video/mpeg4"));
		mimeTypeMap.insert(pair<string, string>(".mac", "application/x-mac"));
		mimeTypeMap.insert(pair<string, string>(".man", "application/x-troff-man"));
		mimeTypeMap.insert(pair<string, string>(".math", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".mdb", "application/msaccess"));
		mimeTypeMap.insert(pair<string, string>(".mdb", "application/x-mdb"));
		mimeTypeMap.insert(pair<string, string>(".mfp", "application/x-shockwave-flash"));
		mimeTypeMap.insert(pair<string, string>(".mht", "message/rfc822"));
		mimeTypeMap.insert(pair<string, string>(".mhtml", "message/rfc822"));
		mimeTypeMap.insert(pair<string, string>(".mi", "application/x-mi"));
		mimeTypeMap.insert(pair<string, string>(".mid", "audio/mid"));
		mimeTypeMap.insert(pair<string, string>(".midi", "audio/mid"));
		mimeTypeMap.insert(pair<string, string>(".mil", "application/x-mil"));
		mimeTypeMap.insert(pair<string, string>(".mml", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".mnd", "audio/x-musicnet-download"));
		mimeTypeMap.insert(pair<string, string>(".mns", "audio/x-musicnet-stream"));
		mimeTypeMap.insert(pair<string, string>(".mocha", "application/x-javascript"));
		mimeTypeMap.insert(pair<string, string>(".movie", "video/x-sgi-movie"));
		mimeTypeMap.insert(pair<string, string>(".mp1", "audio/mp1"));
		mimeTypeMap.insert(pair<string, string>(".mp2", "audio/mp2"));
		mimeTypeMap.insert(pair<string, string>(".mp2v", "video/mpeg"));
		mimeTypeMap.insert(pair<string, string>(".mp3", "audio/mp3"));
		mimeTypeMap.insert(pair<string, string>(".mp4", "video/mpeg4"));
		mimeTypeMap.insert(pair<string, string>(".mpa", "video/x-mpg"));
		mimeTypeMap.insert(pair<string, string>(".mpd", "application/vnd.ms-project"));
		mimeTypeMap.insert(pair<string, string>(".mpe", "video/x-mpeg"));
		mimeTypeMap.insert(pair<string, string>(".mpeg", "video/mpg"));
		mimeTypeMap.insert(pair<string, string>(".mpg", "video/mpg"));
		mimeTypeMap.insert(pair<string, string>(".mpga", "audio/rn-mpeg"));
		mimeTypeMap.insert(pair<string, string>(".mpp", "application/vnd.ms-project"));
		mimeTypeMap.insert(pair<string, string>(".mps", "video/x-mpeg"));
		mimeTypeMap.insert(pair<string, string>(".mpt", "application/vnd.ms-project"));
		mimeTypeMap.insert(pair<string, string>(".mpv", "video/mpg"));
		mimeTypeMap.insert(pair<string, string>(".mpv2", "video/mpeg"));
		mimeTypeMap.insert(pair<string, string>(".mpw", "application/vnd.ms-project"));
		mimeTypeMap.insert(pair<string, string>(".mpx", "application/vnd.ms-project"));
		mimeTypeMap.insert(pair<string, string>(".mtx", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".mxp", "application/x-mmxp"));
		mimeTypeMap.insert(pair<string, string>(".net", "image/pnetvue"));
		mimeTypeMap.insert(pair<string, string>(".nrf", "application/x-nrf"));
		mimeTypeMap.insert(pair<string, string>(".nws", "message/rfc822"));
		mimeTypeMap.insert(pair<string, string>(".odc", "text/x-ms-odc"));
		mimeTypeMap.insert(pair<string, string>(".out", "application/x-out"));
		mimeTypeMap.insert(pair<string, string>(".p10", "application/pkcs10"));
		mimeTypeMap.insert(pair<string, string>(".p12", "application/x-pkcs12"));
		mimeTypeMap.insert(pair<string, string>(".p7b", "application/x-pkcs7-certificates"));
		mimeTypeMap.insert(pair<string, string>(".p7c", "application/pkcs7-mime"));
		mimeTypeMap.insert(pair<string, string>(".p7m", "application/pkcs7-mime"));
		mimeTypeMap.insert(pair<string, string>(".p7r", "application/x-pkcs7-certreqresp"));
		mimeTypeMap.insert(pair<string, string>(".p7s", "application/pkcs7-signature"));
		mimeTypeMap.insert(pair<string, string>(".pc5", "application/x-pc5"));
		mimeTypeMap.insert(pair<string, string>(".pci", "application/x-pci"));
		mimeTypeMap.insert(pair<string, string>(".pcl", "application/x-pcl"));
		mimeTypeMap.insert(pair<string, string>(".pcx", "application/x-pcx"));
		mimeTypeMap.insert(pair<string, string>(".pdf", "application/pdf"));
		mimeTypeMap.insert(pair<string, string>(".pdf", "application/pdf"));
		mimeTypeMap.insert(pair<string, string>(".pdx", "application/vnd.adobe.pdx"));
		mimeTypeMap.insert(pair<string, string>(".pfx", "application/x-pkcs12"));
		mimeTypeMap.insert(pair<string, string>(".pgl", "application/x-pgl"));
		mimeTypeMap.insert(pair<string, string>(".pic", "application/x-pic"));
		mimeTypeMap.insert(pair<string, string>(".pko", "application/vnd.ms-pki.pko"));
		mimeTypeMap.insert(pair<string, string>(".pl", "application/x-perl"));
		mimeTypeMap.insert(pair<string, string>(".plg", "text/html"));
		mimeTypeMap.insert(pair<string, string>(".pls", "audio/scpls"));
		mimeTypeMap.insert(pair<string, string>(".plt", "application/x-plt"));
		mimeTypeMap.insert(pair<string, string>(".png", "image/png"));
		mimeTypeMap.insert(pair<string, string>(".png", "application/x-png"));
		mimeTypeMap.insert(pair<string, string>(".pot", "application/vnd.ms-powerpoint"));
		mimeTypeMap.insert(pair<string, string>(".ppa", "application/vnd.ms-powerpoint"));
		mimeTypeMap.insert(pair<string, string>(".ppm", "application/x-ppm"));
		mimeTypeMap.insert(pair<string, string>(".pps", "application/vnd.ms-powerpoint"));
		mimeTypeMap.insert(pair<string, string>(".ppt", "application/vnd.ms-powerpoint"));
		mimeTypeMap.insert(pair<string, string>(".ppt", "application/x-ppt"));
		mimeTypeMap.insert(pair<string, string>(".pr", "application/x-pr"));
		mimeTypeMap.insert(pair<string, string>(".prf", "application/pics-rules"));
		mimeTypeMap.insert(pair<string, string>(".prn", "application/x-prn"));
		mimeTypeMap.insert(pair<string, string>(".prt", "application/x-prt"));
		mimeTypeMap.insert(pair<string, string>(".ps", "application/x-ps"));
		mimeTypeMap.insert(pair<string, string>(".ps", "application/postscript"));
		mimeTypeMap.insert(pair<string, string>(".ptn", "application/x-ptn"));
		mimeTypeMap.insert(pair<string, string>(".pwz", "application/vnd.ms-powerpoint"));
		mimeTypeMap.insert(pair<string, string>(".r3t", "text/vnd.rn-realtext3d"));
		mimeTypeMap.insert(pair<string, string>(".ra", "audio/vnd.rn-realaudio"));
		mimeTypeMap.insert(pair<string, string>(".ram", "audio/x-pn-realaudio"));
		mimeTypeMap.insert(pair<string, string>(".ras", "application/x-ras"));
		mimeTypeMap.insert(pair<string, string>(".rat", "application/rat-file"));
		mimeTypeMap.insert(pair<string, string>(".rdf", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".rec", "application/vnd.rn-recording"));
		mimeTypeMap.insert(pair<string, string>(".red", "application/x-red"));
		mimeTypeMap.insert(pair<string, string>(".rgb", "application/x-rgb"));
		mimeTypeMap.insert(pair<string, string>(".rjs", "application/vnd.rn-realsystem-rjs"));
		mimeTypeMap.insert(pair<string, string>(".rjt", "application/vnd.rn-realsystem-rjt"));
		mimeTypeMap.insert(pair<string, string>(".rlc", "application/x-rlc"));
		mimeTypeMap.insert(pair<string, string>(".rle", "application/x-rle"));
		mimeTypeMap.insert(pair<string, string>(".rm", "application/vnd.rn-realmedia"));
		mimeTypeMap.insert(pair<string, string>(".rmf", "application/vnd.adobe.rmf"));
		mimeTypeMap.insert(pair<string, string>(".rmi", "audio/mid"));
		mimeTypeMap.insert(pair<string, string>(".rmj", "application/vnd.rn-realsystem-rmj"));
		mimeTypeMap.insert(pair<string, string>(".rmm", "audio/x-pn-realaudio"));
		mimeTypeMap.insert(pair<string, string>(".rmp", "application/vnd.rn-rn_music_package"));
		mimeTypeMap.insert(pair<string, string>(".rms", "application/vnd.rn-realmedia-secure"));
		mimeTypeMap.insert(pair<string, string>(".rmvb", "application/vnd.rn-realmedia-vbr"));
		mimeTypeMap.insert(pair<string, string>(".rmx", "application/vnd.rn-realsystem-rmx"));
		mimeTypeMap.insert(pair<string, string>(".rnx", "application/vnd.rn-realplayer"));
		mimeTypeMap.insert(pair<string, string>(".rp", "image/vnd.rn-realpix"));
		mimeTypeMap.insert(pair<string, string>(".rpm", "audio/x-pn-realaudio-plugin"));
		mimeTypeMap.insert(pair<string, string>(".rsml", "application/vnd.rn-rsml"));
		mimeTypeMap.insert(pair<string, string>(".rt", "text/vnd.rn-realtext"));
		mimeTypeMap.insert(pair<string, string>(".rtf", "application/msword"));
		mimeTypeMap.insert(pair<string, string>(".rtf", "application/x-rtf"));
		mimeTypeMap.insert(pair<string, string>(".rv", "video/vnd.rn-realvideo"));
		mimeTypeMap.insert(pair<string, string>(".sam", "application/x-sam"));
		mimeTypeMap.insert(pair<string, string>(".sat", "application/x-sat"));
		mimeTypeMap.insert(pair<string, string>(".sdp", "application/sdp"));
		mimeTypeMap.insert(pair<string, string>(".sdw", "application/x-sdw"));
		mimeTypeMap.insert(pair<string, string>(".sit", "application/x-stuffit"));
		mimeTypeMap.insert(pair<string, string>(".slb", "application/x-slb"));
		mimeTypeMap.insert(pair<string, string>(".sld", "application/x-sld"));
		mimeTypeMap.insert(pair<string, string>(".slk", "drawing/x-slk"));
		mimeTypeMap.insert(pair<string, string>(".smi", "application/smil"));
		mimeTypeMap.insert(pair<string, string>(".smil", "application/smil"));
		mimeTypeMap.insert(pair<string, string>(".smk", "application/x-smk"));
		mimeTypeMap.insert(pair<string, string>(".snd", "audio/basic"));
		mimeTypeMap.insert(pair<string, string>(".sol", "text/plain"));
		mimeTypeMap.insert(pair<string, string>(".sor", "text/plain"));
		mimeTypeMap.insert(pair<string, string>(".spc", "application/x-pkcs7-certificates"));
		mimeTypeMap.insert(pair<string, string>(".spl", "application/futuresplash"));
		mimeTypeMap.insert(pair<string, string>(".spp", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".ssm", "application/streamingmedia"));
		mimeTypeMap.insert(pair<string, string>(".sst", "application/vnd.ms-pki.certstore"));
		mimeTypeMap.insert(pair<string, string>(".stl", "application/vnd.ms-pki.stl"));
		mimeTypeMap.insert(pair<string, string>(".stm", "text/html"));
		mimeTypeMap.insert(pair<string, string>(".sty", "application/x-sty"));
		mimeTypeMap.insert(pair<string, string>(".svg", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".swf", "application/x-shockwave-flash"));
		mimeTypeMap.insert(pair<string, string>(".tdf", "application/x-tdf"));
		mimeTypeMap.insert(pair<string, string>(".tg4", "application/x-tg4"));
		mimeTypeMap.insert(pair<string, string>(".tga", "application/x-tga"));
		mimeTypeMap.insert(pair<string, string>(".tif", "image/tiff"));
		mimeTypeMap.insert(pair<string, string>(".tif", "application/x-tif"));
		mimeTypeMap.insert(pair<string, string>(".tiff", "image/tiff"));
		mimeTypeMap.insert(pair<string, string>(".tld", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".top", "drawing/x-top"));
		mimeTypeMap.insert(pair<string, string>(".torrent", "application/x-bittorrent"));
		mimeTypeMap.insert(pair<string, string>(".tsd", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".txt", "text/plain"));
		mimeTypeMap.insert(pair<string, string>(".uin", "application/x-icq"));
		mimeTypeMap.insert(pair<string, string>(".uls", "text/iuls"));
		mimeTypeMap.insert(pair<string, string>(".vcf", "text/x-vcard"));
		mimeTypeMap.insert(pair<string, string>(".vda", "application/x-vda"));
		mimeTypeMap.insert(pair<string, string>(".vdx", "application/vnd.visio"));
		mimeTypeMap.insert(pair<string, string>(".vml", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".vpg", "application/x-vpeg005"));
		mimeTypeMap.insert(pair<string, string>(".vsd", "application/vnd.visio"));
		mimeTypeMap.insert(pair<string, string>(".vsd", "application/x-vsd"));
		mimeTypeMap.insert(pair<string, string>(".vss", "application/vnd.visio"));
		mimeTypeMap.insert(pair<string, string>(".vst", "application/vnd.visio"));
		mimeTypeMap.insert(pair<string, string>(".vst", "application/x-vst"));
		mimeTypeMap.insert(pair<string, string>(".vsw", "application/vnd.visio"));
		mimeTypeMap.insert(pair<string, string>(".vsx", "application/vnd.visio"));
		mimeTypeMap.insert(pair<string, string>(".vtx", "application/vnd.visio"));
		mimeTypeMap.insert(pair<string, string>(".vxml", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".wav", "audio/wav"));
		mimeTypeMap.insert(pair<string, string>(".wax", "audio/x-ms-wax"));
		mimeTypeMap.insert(pair<string, string>(".wb1", "application/x-wb1"));
		mimeTypeMap.insert(pair<string, string>(".wb2", "application/x-wb2"));
		mimeTypeMap.insert(pair<string, string>(".wb3", "application/x-wb3"));
		mimeTypeMap.insert(pair<string, string>(".wbmp", "image/vnd.wap.wbmp"));
		mimeTypeMap.insert(pair<string, string>(".wiz", "application/msword"));
		mimeTypeMap.insert(pair<string, string>(".wk3", "application/x-wk3"));
		mimeTypeMap.insert(pair<string, string>(".wk4", "application/x-wk4"));
		mimeTypeMap.insert(pair<string, string>(".wkq", "application/x-wkq"));
		mimeTypeMap.insert(pair<string, string>(".wks", "application/x-wks"));
		mimeTypeMap.insert(pair<string, string>(".wm", "video/x-ms-wm"));
		mimeTypeMap.insert(pair<string, string>(".wma", "audio/x-ms-wma"));
		mimeTypeMap.insert(pair<string, string>(".wmd", "application/x-ms-wmd"));
		mimeTypeMap.insert(pair<string, string>(".wmf", "application/x-wmf"));
		mimeTypeMap.insert(pair<string, string>(".wml", "text/vnd.wap.wml"));
		mimeTypeMap.insert(pair<string, string>(".wmv", "video/x-ms-wmv"));
		mimeTypeMap.insert(pair<string, string>(".wmx", "video/x-ms-wmx"));
		mimeTypeMap.insert(pair<string, string>(".wmz", "application/x-ms-wmz"));
		mimeTypeMap.insert(pair<string, string>(".wp6", "application/x-wp6"));
		mimeTypeMap.insert(pair<string, string>(".wpd", "application/x-wpd"));
		mimeTypeMap.insert(pair<string, string>(".wpg", "application/x-wpg"));
		mimeTypeMap.insert(pair<string, string>(".wpl", "application/vnd.ms-wpl"));
		mimeTypeMap.insert(pair<string, string>(".wq1", "application/x-wq1"));
		mimeTypeMap.insert(pair<string, string>(".wr1", "application/x-wr1"));
		mimeTypeMap.insert(pair<string, string>(".wri", "application/x-wri"));
		mimeTypeMap.insert(pair<string, string>(".wrk", "application/x-wrk"));
		mimeTypeMap.insert(pair<string, string>(".ws", "application/x-ws"));
		mimeTypeMap.insert(pair<string, string>(".ws2", "application/x-ws"));
		mimeTypeMap.insert(pair<string, string>(".wsc", "text/scriptlet"));
		mimeTypeMap.insert(pair<string, string>(".wsdl", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".wvx", "video/x-ms-wvx"));
		mimeTypeMap.insert(pair<string, string>(".xdp", "application/vnd.adobe.xdp"));
		mimeTypeMap.insert(pair<string, string>(".xdr", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xfd", "application/vnd.adobe.xfd"));
		mimeTypeMap.insert(pair<string, string>(".xfdf", "application/vnd.adobe.xfdf"));
		mimeTypeMap.insert(pair<string, string>(".xhtml", "text/html"));
		mimeTypeMap.insert(pair<string, string>(".xls", "application/vnd.ms-excel"));
		mimeTypeMap.insert(pair<string, string>(".xls", "application/x-xls"));
		mimeTypeMap.insert(pair<string, string>(".xlw", "application/x-xlw"));
		mimeTypeMap.insert(pair<string, string>(".xml", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xpl", "audio/scpls"));
		mimeTypeMap.insert(pair<string, string>(".xq", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xql", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xquery", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xsd", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xsl", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xslt", "text/xml"));
		mimeTypeMap.insert(pair<string, string>(".xwd", "application/x-xwd"));
		mimeTypeMap.insert(pair<string, string>(".x_b", "application/x-x_b"));
		mimeTypeMap.insert(pair<string, string>(".sis", "application/vnd.symbian.install"));
		mimeTypeMap.insert(pair<string, string>(".sisx", "application/vnd.symbian.install"));
		mimeTypeMap.insert(pair<string, string>(".x_t", "application/x-x_t"));
		mimeTypeMap.insert(pair<string, string>(".ipa", "application/vnd.iphone"));
		mimeTypeMap.insert(pair<string, string>(".apk", "application/vnd.android.package-archive"));
		mimeTypeMap.insert(pair<string, string>(".xap", "application/x-silverlight-app"));
	}
}
void WSHTTP::Util::changeTerminalCursor(int n)
{
	CONSOLE_CURSOR_INFO cursor_info = { 1,n };
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}
/**
 * set the output text color from terminal
 */
void WSHTTP::Util::setTerminalOutputColor(unsigned short forecolor, unsigned short backgroudcolor)
{
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE); //get handle
	SetConsoleTextAttribute(hCon, forecolor | backgroudcolor); //set color
}

void WSHTTP::Util::enableLog(bool enable)
{
	openLog = enable;
}

void WSHTTP::Util::ROLLBACK_LOG(Log_Level l, string tag, string msg, int rollbackSize, bool newline)
{
	if (!openLog)return;
	string rbCommand;
	while (rollbackSize > 0) { rbCommand += "\b"; --rollbackSize; }
	cout << rbCommand;
	LOG(l, tag, msg, true, newline);
}

string WSHTTP::Util::getMimeType(const string & suffix)
{
	if (mimeTypeMap.size() == 0) {
		fillFileMimeTypeMap();
	}
	if (mimeTypeMap.count(suffix) > 0) {
		return mimeTypeMap[suffix];
	}
	//unknown suffix
	return "application/octet-stream";//default mimetype
}

string WSHTTP::Util::getFileType(const string & mimeType)
{
	if (mimeTypeMap.size() == 0) {
		fillFileMimeTypeMap();
	}
	map<string, string>::iterator it = mimeTypeMap.end();
	it = std::find_if(mimeTypeMap.begin(), mimeTypeMap.end(), map_value_finder(mimeType));
	if (it == mimeTypeMap.end()) {
		//unknown fileType
		return ".unknown";//default fileType
	}else {
		return it->first;
	}
}

/**
 * log function
 */
void WSHTTP::Util::LOG(Log_Level level, string tag, string msg,
	bool rollback, bool newline)
{
	if (!openLog)return;
	string BANNER;
	if (!rollback) {
		switch (level) {
		case L_INFO:
			setTerminalOutputColor(COMMON_LVB_REVERSE_VIDEO, BACKGROUND_GREEN);
			BANNER = ">>>>===WSHTTP-LOGGER===<<<<[INFO]";
			cout << BANNER;
			break;
		case L_WARNNING:
			setTerminalOutputColor(COMMON_LVB_REVERSE_VIDEO, BACKGROUND_GREEN | BACKGROUND_RED);
			BANNER = ">>>>===WSHTTP-LOGGER===<<<<[WARNNING]";
			cout << BANNER;
			break;
		case L_ERROR:
			setTerminalOutputColor(COMMON_LVB_REVERSE_VIDEO, BACKGROUND_RED);
			BANNER = ">>>>===WSHTTP-LOGGER===<<<<[ERROR]";
			cout << BANNER;
			break;
		default:
			setTerminalOutputColor(COMMON_LVB_REVERSE_VIDEO, BACKGROUND_INTENSITY | BACKGROUND_BLUE
				| BACKGROUND_GREEN | BACKGROUND_RED);
			BANNER = ">>>>===WSHTTP-LOGGER===<<<<";
			cout << BANNER;
			break;
		}
		cout << "[" << tag << "] ";
	}
	setTerminalOutputColor(COMMON_LVB_REVERSE_VIDEO, BACKGROUND_INTENSITY | BACKGROUND_BLUE
		| BACKGROUND_GREEN | BACKGROUND_RED);
	cout << msg;
	if (newline) {
		cout << endl;
	}
}

string WSHTTP::Util::utf82String(string & str)
{
	int nwLen = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, NULL, 0);
	wchar_t* pwBuf = new wchar_t[nwLen + 1];//一定要加1，不然会出现尾巴
	memset(pwBuf, 0, nwLen * 2 + 2);
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length(), pwBuf, nwLen);
	int nLen = WideCharToMultiByte(CP_ACP, 0, pwBuf, -1, NULL, NULL, NULL, NULL);
	char* pBuf = new char[nLen + 1];
	memset(pBuf, 0, nLen + 1);
	WideCharToMultiByte(CP_ACP, 0, pwBuf, nwLen, pBuf, nLen, NULL, NULL);
	std::string retStr = pBuf;
	delete[]pBuf;
	delete[]pwBuf;
	pBuf = NULL;
	pwBuf = NULL;
	return retStr;
}

string WSHTTP::Util::string2Utf8(string & str)
{
	int nwLen = ::MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);
	wchar_t* pwBuf = new wchar_t[nwLen + 1];//一定要加1，不然会出现尾巴 
	ZeroMemory(pwBuf, nwLen * 2 + 2);
	::MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.length(), pwBuf, nwLen);
	int nLen = ::WideCharToMultiByte(CP_UTF8, 0, pwBuf, -1, NULL, NULL, NULL, NULL);
	char* pBuf = new char[nLen + 1];
	ZeroMemory(pBuf, nLen + 1);
	::WideCharToMultiByte(CP_UTF8, 0, pwBuf, nwLen, pBuf, nLen, NULL, NULL);
	std::string retStr(pBuf);
	delete[]pwBuf;
	delete[]pBuf;
	pwBuf = NULL;
	pBuf = NULL;
	return retStr;
}

void WSHTTP::Util::toUpperString(string & str)
{
	transform(str.begin(), str.end(), str.begin(), ::toupper);
}

void WSHTTP::Util::toLowerString(string & str)
{
	transform(str.begin(), str.end(), str.begin(), ::tolower);
}

time_t WSHTTP::Util::convertCookieDate(const string & cookieDate)
{
	int d, y, h, m, s;
	char wbuf[5], mbuf[4];
	sscanf_s(cookieDate.c_str(), "%s %02d %s %d %02d:%02d:%02d", wbuf, sizeof(wbuf),
		&d,
		mbuf, sizeof(mbuf),
		&y, &h, &m, &s);
	mbuf[3] = '\0';
	char mmmMM[48] = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec";
	int month = (strstr(mmmMM, mbuf) - mmmMM) / 4 + 1;
	tm _t;
	_t.tm_mday = d;
	_t.tm_mon = month - 1;
	_t.tm_year = y - 1900;
	_t.tm_hour = h;
	_t.tm_min = m;
	_t.tm_sec = s;
	//_t.tm_isdst = 0;
	time_t time_sec = mktime(&_t);
	return time_sec;
}

string WSHTTP::Util::convertTime_T(time_t t)
{
	struct tm p;
	gmtime_s(&p, &t);
	char s[50];
	strftime(s, sizeof(s), "%a, %d %b %Y %H:%M:%S GMT", &p);
	return string(s);
}

time_t WSHTTP::Util::nowTime()
{
	time_t _t;
	time(&_t);
	return _t;
}

vector<string> WSHTTP::Util::split(const string & srcstr, const string & delimeter)
{
	vector<string> ret(0);//use ret save the spilted reault
	if (srcstr.empty())    //judge the arguments
	{
		return ret;
	}
	string::size_type pos_begin = srcstr.find_first_not_of(delimeter);//find first element of srcstr

	string::size_type dlm_pos;//the delimeter postion
	string temp;              //use third-party temp to save splited element
	while (pos_begin != string::npos)//if not a next of end, continue spliting
	{
		dlm_pos = srcstr.find(delimeter, pos_begin);//find the delimeter symbol
		if (dlm_pos != string::npos)
		{
			temp = srcstr.substr(pos_begin, dlm_pos - pos_begin);
			pos_begin = dlm_pos + delimeter.length();
		}
		else
		{
			temp = srcstr.substr(pos_begin);
			pos_begin = dlm_pos;
		}
		if (!temp.empty())
			ret.push_back(temp);
	}
	return ret;
}

bool WSHTTP::Util::startsWith(const std::string & str, const std::string & start)
{
	int srclen = str.size();
	int startlen = start.size();
	if (srclen >= startlen) {
		string temp = str.substr(0, startlen);
		if (temp == start)
			return true;
	}
	return false;
}



bool WSHTTP::Util::endsWith(const std::string & str, const std::string & end)
{
	int srclen = str.size();
	int endlen = end.size();
	if (srclen >= endlen)
	{
		string temp = str.substr(srclen - endlen, endlen);
		if (temp == end)
			return true;
	}
	return false;
}

string WSHTTP::Util::trim(const std::string & str_)
{
	string str(str_);
	if (str.empty())
	{
		return "";
	}
	string::const_iterator ch = str.begin();
	while (' ' == *ch || '\r' == *ch || '\n' == *ch || '\t' == *ch || '\f' == *ch)
	{
		str.erase(0, str.find_first_not_of(*ch));

		ch = str.begin();
	}
	ch = str.end() - 1;
	while (' ' == *ch || '\r' == *ch || '\n' == *ch || '\t' == *ch || '\f' == *ch)
	{
		str.erase(str.find_last_not_of(*ch) + 1);

		ch = str.end() - 1;
	}
	return str;
}

string WSHTTP::Util::parseMapDataToText(map<string, string>& params, string split, string link)
{
	if (params.size() == 0)
		return "";
	map<string, string>::iterator it;
	string rs;
	for (it = params.begin(); it != params.end(); ++it) {
		rs += it->first + link + it->second + split;
	}
	return rs;
}

bool WSHTTP::Util::isIPv4Address(const string & input)
{
	string IPV4_BASIC_PATTERN_STRING;
	IPV4_BASIC_PATTERN_STRING.append("(([1-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){1}"); // initial first field, 1-255
	IPV4_BASIC_PATTERN_STRING.append("(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){2}"); // following 2 fields, 0-255 followed by .
	IPV4_BASIC_PATTERN_STRING.append("([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"); // final field, 0-255
	regex reg("^" + IPV4_BASIC_PATTERN_STRING + "$");
	return regex_match(input, reg);
}


bool WSHTTP::Util::isIPv6Address(const string & input)
{
	regex IPV6_STD_PATTERN("^[0-9a-fA-F]{1,4}(:[0-9a-fA-F]{1,4}){7}$");
	if (regex_match(input, IPV6_STD_PATTERN)) {
		return true;
	}
	else {
		size_t colonCount = 0;
		for (size_t i = 0; i < input.length(); i++) {
			if (input[i] == ':') {
				colonCount++;
			}
		}
		string IPV6_HEX_COMPRESSED_PATTERN_STRING;
		IPV6_HEX_COMPRESSED_PATTERN_STRING.append("^(([0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,5})?)"); // 0-6 hex fields
		IPV6_HEX_COMPRESSED_PATTERN_STRING.append("::");
		IPV6_HEX_COMPRESSED_PATTERN_STRING.append("(([0-9A-Fa-f]{1,4}(:[0-9A-Fa-f]{1,4}){0,5})?)$"); // 0-6 hex fields
		regex IPV6_HEX_COMPRESSED_PATTERN(IPV6_HEX_COMPRESSED_PATTERN_STRING);
		return  colonCount <= 7 && regex_match(input, IPV6_HEX_COMPRESSED_PATTERN);
	}
}
std::string WSHTTP::Util::stringifyFileSize(uint64_t bytes)
{
	const char *suffix[] = { "B", "KB", "MB", "GB", "TB" };
	char length = sizeof(suffix) / sizeof(suffix[0]);

	int i = 0;
	double dblBytes = bytes;

	if (bytes > 1024) {
		for (i = 0; (bytes / 1024) > 0 && i < length - 1; i++, bytes /= 1024)
			dblBytes = bytes / 1024.0;
	}

	static char output[200];
	sprintf_s(output, "%.02lf %s", dblBytes, suffix[i]);
	return string(output);
}

std::string WSHTTP::Util::d2string(double n, int precision)
{
	string s = to_string(n);
	int pos = s.find(".", sizeof(char));
	int i = s.length() - 1 - pos - precision;
	while (i--)
		s.pop_back();
	return s;
}


WSHTTP::Util::map_value_finder::
map_value_finder(const string & cmp_string) :m_s_cmp_string(cmp_string)
{
}

bool WSHTTP::Util::map_value_finder::operator()(const map<string, string>::value_type & pair)
{
	return pair.second == m_s_cmp_string;
}
