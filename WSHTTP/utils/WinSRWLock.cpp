﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:ReadWriteLock in WIN OS.Use in Multi-
*|             Threading env.[UN-EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2021-09-16 13:19
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "WinSRWLock.h"

WSHTTP::WinSRWLock::WinSRWLock()
{
	_lock_mask = NOT_INIT;
}


void WSHTTP::WinSRWLock::initLock()
{
	InitializeSRWLock(&_srwLock);
	_lock_mask = UNLOCK;
}

void WSHTTP::WinSRWLock::readLock()
{
	AcquireSRWLockShared(&_srwLock);
	_lock_mask = READ_LOCK;
}

void WSHTTP::WinSRWLock::writeLock()
{
	AcquireSRWLockExclusive(&_srwLock);
	_lock_mask = WRITE_LOCK;
}

void WSHTTP::WinSRWLock::readUnlock()
{
	ReleaseSRWLockShared(&_srwLock);
	_lock_mask = UNLOCK;
}

void WSHTTP::WinSRWLock::writeUnlock()
{
	ReleaseSRWLockExclusive(&_srwLock);
	_lock_mask = UNLOCK;
}

WSHTTP::MASK WSHTTP::WinSRWLock::getLockMask()
{
	return _lock_mask;
}
