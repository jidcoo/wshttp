﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:WSHTTP Utils Set [Static Class][UN-EXPORTED]
*|　Author:Jidcoo 
*|　Version:1.1
*|　Date:2022-06-27 11:16
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:trim()方法重构功能为删除字符串开头和结尾的不可见字符
*|  Date:2022-06-27 11:16
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "../framework.h"
#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <regex>
#include <algorithm>
#include <ctime>		
#include <set>
#include <string.h>
#include <sstream>
#include <cstring>
#include <iostream>
#include <sstream>
#include <strstream>
#include <fstream>
#include <regex>
#include <list>
using namespace std;
namespace WSHTTP{
/*Log_Level*/
typedef int Log_Level;
#define L_INFO 1<<1
#define L_WARNNING 1<<2
#define L_ERROR 1<<3
	class Util {
	private:
		Util();
		~Util();
	private:
		class map_value_finder{
		public:
			map_value_finder(const string &cmp_string);
			bool operator ()(const map<string, string>::value_type &pair);
		private:
			const std::string &m_s_cmp_string;
		};
	private:
		static bool openLog;
		static map<string, string> mimeTypeMap;
	private:
		//set the output text color from terminal
		static void setTerminalOutputColor(unsigned short forecolor = 4, unsigned short backgroudcolor = 0);
		static map<string, string> initFileMimeTypeMap();
		static void fillFileMimeTypeMap();
	public:
		//隐藏光标只需要把n设置为0也就是把上面的结构体的bVisible设置为false就可以了
		static void changeTerminalCursor(int n);
		//output log  
		static __declspec(dllexport) void enableLog(bool enable);
		static void ROLLBACK_LOG(Log_Level, string tag, string msg, int rollbackSize, bool newline = false);
		static string getMimeType(const string &suffix);
		static string getFileType(const string &mimeType);
		static void LOG(Log_Level, string tag, string msg, bool rollback = false, bool newline = true);
		static string utf82String(string &str);
		static string string2Utf8(string &str);
		static void toUpperString(string &str);
		static void toLowerString(string &str);
		static time_t convertCookieDate(const string& 
			cookieDate/** datestr format e.g:Thu, 01 Jan 2025 00:00:00 GMT */);
		static string convertTime_T(time_t t);
		static time_t nowTime();
		//split the string by delimeter
		static vector<string> split(const string& srcstr, const string& delimeter = ";");
		static bool startsWith(const std::string& str, const std::string& start);
		static bool endsWith(const std::string& str, const std::string& end);
		//handle the start and the end
		static string trim(const std::string& str);
		static string parseMapDataToText(map<string, string> &params, string split, string link);
		static bool isIPv4Address(const string& input);
		static bool isIPv6Address(const string& input);
		static std::string stringifyFileSize(uint64_t size);
		static std::string d2string(double n, int precision);
	};
};
