﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:HttpGet:Mapping "GET" Request [EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2021-09-16 20:12
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpGet.h"
WSHTTP::HttpGet::HttpGet()
	:HttpMethod() {
	METHOD = "GET";
}

WSHTTP::HttpGet::HttpGet(string  path)
	: HttpMethod(path) {
	METHOD = "GET";
}

WSHTTP::HttpGet::~HttpGet()
{
	Util::LOG(L_INFO, TAG, "HttpGet destroyed:" + getRequestUrl());
}


