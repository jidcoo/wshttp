﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:HttpMethod [EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2021-09-16 20:42
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpMethod.h"

WSHTTP::HttpMethod::HttpMethod()
{
	path = "";
}

WSHTTP::HttpMethod::HttpMethod(string & _path)
{
	//set path
	auto url = CURL::_parse_(_path);
	string path = url.getPath();
	if (!path.empty()) {
		if(path[0] != '/') {
			path = "/" + path;
		}
		path += "?"+url.getQuery();
		this->path = path;
	}
}

WSHTTP::HttpMethod::~HttpMethod()
{
}

const string WSHTTP::HttpMethod::getRequestUrl()
{
	if(this->_cur_curl){
		return this->_cur_curl->getScheme() + "://" + this->_cur_curl->toString();
	}
	return "";
}

void WSHTTP::HttpMethod::addRequestHeader(string key, string vaL)
{
	string tmp(key);
	string COOKIE("COOKIE");
	Util::toUpperString(tmp);
	if (tmp == COOKIE) {
		Util::LOG(L_WARNNING, TAG, "The cookie field will be ignore,please call the method addCookie() to add cookie");
	}else {
		req_headers[key] = vaL;
	}
}

string WSHTTP::HttpMethod::getRequestHeader(string key)
{
	if (req_headers.count(key) > 0) {
		return req_headers[key];
	}
	return "";
}

map<string, string>& WSHTTP::HttpMethod::getRequestHeaders()
{
	return req_headers;
}

void WSHTTP::HttpMethod::removeRequestHeader(string key)
{
	req_headers.erase(key);
}

void WSHTTP::HttpMethod::addRequestCookie(string key, string val)
{
	req_cookies[key] = val;
}

string WSHTTP::HttpMethod::getRequestCookie(string key)
{
	if (req_cookies.count(key) > 0) {
		return req_cookies[key];
	}
	return "";
}

map<string, string>& WSHTTP::HttpMethod::getRequestCookies()
{
	return req_cookies;
}

void WSHTTP::HttpMethod::removeRequestCookie(string key)
{
	req_cookies.erase(key);
}

void WSHTTP::HttpMethod::setAutoAttachCookiesFromLastResponse(bool isAutoAttachCookie)
{
	this->autoAttachCookies = isAutoAttachCookie;
}

bool WSHTTP::HttpMethod::isAutoAttachCookiesFromLastResponse()
{
	return autoAttachCookies;
}

void WSHTTP::HttpMethod::setCURL(CURL* curl)
{
	if (curl!=nullptr) {
		if (!path.empty()) {
			curl->setPath(path);
		}
		this->_cur_curl = curl;
	}
}


