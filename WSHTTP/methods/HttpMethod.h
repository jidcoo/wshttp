﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:HttpMethod [EXPORTED]                                               
*|　Author:Jidcoo                                                
*|　Version:1.0                                                 
*|　Date:2021-09-16 20:42                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:                                                        
*|  Date:                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "../compents/CURL.h"
#include "../compents/Cookie.h"
using namespace std;
namespace WSHTTP {
	class WSHTTP_API HttpMethod {
	private:
		const string TAG = "HttpMethod";
	protected:
		string METHOD;
	public:
		friend class CookieHandler;
		friend class RequestHandler;
		friend class ResponseHandler;
		friend class HttpGetRequestHandler;
		friend class HttpHeaderHandler;
	public:
		HttpMethod();
		HttpMethod(string& path);
		virtual ~HttpMethod();
	public:
		/* get the http fully req url */
		const string getRequestUrl();
		/* add a request header */
		void addRequestHeader(string key, string vaL);
		/* get request header by key */
		string getRequestHeader(string key);
		/* get all req headers*/
		map<string, string>& getRequestHeaders();
		/* remove request header by key if key exist */
		void removeRequestHeader(string key);
		/* add a request cookie */
		void addRequestCookie(string key, string val);
		/* get request cookie by key */
		string getRequestCookie(string key);
		/* get all req cookies*/
		map<string, string>& getRequestCookies();
		/* remove request cookie by key if key exist */
		void removeRequestCookie(string key);
		/* set the cookie from response */
		void setAutoAttachCookiesFromLastResponse(bool isAutoAttachCookie);
		bool isAutoAttachCookiesFromLastResponse();
	protected:
		bool autoAttachCookies;
		map<string, string> req_headers;
		map<string, string> req_cookies;
	private:
		void setCURL(CURL* curl);
	private:
		string _req_cookies_string;
		CURL* _cur_curl;
		string path;
	};
};
