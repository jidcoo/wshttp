﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:HttpGet:Mapping "GET" Request [EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2021-09-16 20:12
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "HttpMethod.h"
namespace WSHTTP {
	class WSHTTP_API HttpGet :public HttpMethod {
	public:
		HttpGet();
		HttpGet(string path);
		virtual ~HttpGet();
	private:
		const string TAG = "HttpGet";
		friend class WSHttpRunner;
	};
};



