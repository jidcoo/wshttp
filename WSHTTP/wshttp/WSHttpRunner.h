﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:WSHttpRunner [EXPORTED]                                               
*|　Author:Jidcoo                                                
*|　Version:1.1                                                 
*|　Date:2022-06-28 20:03                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:新增exec接口，将网络请求器的处理请求方式重构为处理器责任链模式，每一个请求拥有独立请求链环境                                                        
*|  Date:2022-06-28 20:03                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "AbstractHttpRunner.h"
#include "../chain/WSHTTPHandleChain.h"
#include "../compents/CookieManager.h"
using namespace std;
namespace WSHTTP {
	class WSHTTP_API WSHttpRunner :public AbstractHttpRunner
	{
	public:
		WSHttpRunner();
		~WSHttpRunner();
		virtual ResponseCaller* run(HttpConnection &, HttpMethod &) override;
	private:
		ResponseCaller* exec(HttpConnection &, HttpMethod &);
	private:
		const string TAG = "WSHttpRunner";
		vector<HttpHandler*> handlerList;
		CookieManager* cookieManager;
	};
};