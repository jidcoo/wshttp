﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:AbstractHttpRunner[Interface] [UN-EXPORTED]                                         
*|　Author:Jidcoo                                                
*|　Version:1.0                                                 
*|　Date:2021-09-20 14:30                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:                                                        
*|  Date:                                               
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "../framework.h"
#include "../compents/ResponseCaller.h"
#include "../compents/HttpConnection.h"
#include "../methods/HttpMethod.h"
namespace WSHTTP {
	/**
	 * AbstractHttpRunner interface
	 */
	class AbstractHttpRunner {
	public:
		/**
		 * run the http method
		 */
		virtual ResponseCaller* run(HttpConnection &, HttpMethod &) = 0;
	protected:
	private:
	};
};

