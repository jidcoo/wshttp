﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:WSHttpRunner [EXPORTED]
*|　Author:Jidcoo
*|　Version:1.1
*|　Date:2022-06-28 20:03
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:新增exec接口，将网络请求器的处理请求方式重构为处理器责任链模式，每一个请求拥有独立请求链环境
*|  Date:2022-06-28 20:03
*-------------------------------------------------------------------------------------------------------------
*/

#include "WSHttpRunner.h"
#include "../chain/RequestHandler.cpp"
#include "../chain/CookieHandler.cpp"
#include "../chain/HttpHeaderHandler.cpp"
#include "../chain/HttpGetRequestHandler.cpp"
#include "../chain/ResponseHandler.cpp"
#include "../chain/ChainEndHandler.cpp"

WSHTTP::WSHttpRunner::WSHttpRunner(){
	Util::changeTerminalCursor(0);
	Util::LOG(L_INFO, TAG, "WSHttpRunner Created [V1.0 @Jidcoo]");
	cookieManager = new CookieManager();
	handlerList.push_back(new RequestHandler());
	handlerList.push_back(new CookieHandler(cookieManager));
	handlerList.push_back(new HttpHeaderHandler());
	handlerList.push_back(new HttpGetRequestHandler());
	handlerList.push_back(new ResponseHandler());
	handlerList.push_back(new ChainEndHandler());
}

WSHTTP::WSHttpRunner::~WSHttpRunner()
{
	Util::changeTerminalCursor(1);
	if(cookieManager!=nullptr){
		delete cookieManager;
	}
	for(auto& handler:handlerList){
		delete handler;
	}
	handlerList.clear();
	Util::LOG(L_INFO, TAG, "WSHttpRunner Destroyed [V1.0 @Jidcoo]");
}

WSHTTP::ResponseCaller * WSHTTP::WSHttpRunner::run(HttpConnection &connection, HttpMethod &request)
{
	return exec(connection, request);
}

WSHTTP::ResponseCaller* WSHTTP::WSHttpRunner::exec(HttpConnection &con, HttpMethod &req)
{
	WSHTTPHandleChain execHandlerChain;
	execHandlerChain.ready();
	ResponseCaller *caller = new ResponseCaller();
	HttpHandler** handler = &handlerList[0];
	bool ret=execHandlerChain.handle(req, con, *caller,handler,nullptr);
	if(ret){
		return caller;
	}
	return nullptr;
}
