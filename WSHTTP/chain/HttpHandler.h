﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:HttpHandler Node & Chain [UN-EXPORTED]                                            
*|　Author: Jidcoo                                                
*|　Version: 1.1                                                 
*|　Date: 2022-06-27 20:32                                                 
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info: 将HttpRequestHandler重命名为HttpHandler，并修改了接口，基于责任链模式重新架构;此类为处理器接口类                                                        
*|  Date: 2022-06-28 19:37                                                  
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "../methods/HttpMethod.h"
#include "../compents/HttpConnection.h"
#include "../compents/ResponseCaller.h"
using namespace std;
namespace WSHTTP {
	class HttpHandler {
	private:
		friend class WSHttpRunner;
		friend class WSHTTPHandleChain;
	private:
		virtual bool handle(HttpMethod& request, HttpConnection& connection,
			ResponseCaller& respCaller, HttpHandler* *handlers, HttpHandler* chain) {
			if (beforeRequest(request, connection, respCaller)) {
				if (chain->handle(request, connection, respCaller, handlers, chain)) {
					if (afterResponse(request, connection, respCaller)) {
						return true;
					}else {
						Util::LOG(L_WARNNING, getTag(), "afterResponse handle failed in handle chain");
						return false;
					}
				}
				else {
					return false;
				}
			}else {
				Util::LOG(L_WARNNING, getTag(), "beforeRequest handle failed in handle chain");
				return false;
			}
		}
		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller)=0;
		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller)=0;
		virtual const string& getTag() const = 0;
	};
};
