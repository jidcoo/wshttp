﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:Http Request Exec Chain Node:CookieHandler [UN-EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2022-06-28 20:09
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "../utils/Util.h"
#include "HttpHandler.h"
#include "../compents/CookieManager.h"
namespace WSHTTP {
	class CookieHandler :public HttpHandler {
	public:
		~CookieHandler(){
			this->cookieManager = nullptr;
		}
	private:
		friend class WSHttpRunner;
		CookieManager* cookieManager;
		const string TAG = "CookieHandler";
		CookieHandler(CookieManager* cookieManager) {
			this->cookieManager = cookieManager;
		}
	private:
		virtual const string& getTag() const override
		{
			return TAG;
		}

		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			if (request.isAutoAttachCookiesFromLastResponse()) {
				//get the context cookies
				vector<Cookie> cookies = cookieManager->getCookies();
				list<Cookie> matchedCookies;
				cdate now = Util::nowTime();
				bool expired = false;
				for (Cookie& cookie : cookies) {
					if (!cookie.isExpired(now)) {
						if (request._cur_curl!=nullptr&&CookieManager::matchCookie(cookie,
							request._cur_curl->getHost(),
							request._cur_curl->getPort(),
							request._cur_curl->getPath(),
							request._cur_curl->isSecure())) {
							//Util::LOG(L_INFO, TAG, "Cookie matched");
							matchedCookies.push_back(cookie);
						}
					}else {
						//Util::LOG(L_WARNNING, TAG, "Cookie expired");
						expired = true;
					}
				}
				// Per RFC 6265, 5.3
				// The user agent must evict all expired cookies if, at any time, an expired cookie
				// exists in the cookie store
				if (expired) {
					cookieManager->clearExpired(now);
				}
				// Generate Cookie request headers
				if (!matchedCookies.empty()) {
					for (auto &cookie : matchedCookies) {
						request.addRequestCookie(cookie.getName(), cookie.getValue());
					}
				}
			}
			if (request.getRequestCookies().size() == 0) {
				request._req_cookies_string ="";
				return true;
			}
			string cookies = "Cookie: ";
			for (const auto& iter : request.getRequestCookies()) {
				cookies += iter.first;
				cookies += "=";
				cookies += iter.second;
				cookies += "; ";
			}
			cookies += "\r\n\r\n";
			request._req_cookies_string = cookies;
			return true;
		}

		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			for (Cookie cookie : respCaller.getResponseCookies()) {
				if (!cookie.isInvalid() && request._cur_curl!=nullptr&&CookieManager::validateCookie(cookie,
					request._cur_curl->getHost(),
					request._cur_curl->getPort(),
					request._cur_curl->getPath(),
					request._cur_curl->isSecure())) {
					//Util::LOG(L_INFO, TAG, "Auto save cookie");
					cookieManager->addCookie(cookie);
				}else {
					//Util::LOG(L_WARNNING, TAG, "Cookie rejected:"+cookie.toString());
				}
			}
			return true;
		}
	};
};