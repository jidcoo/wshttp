﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:Http Request Exec Chain Node:HttpGetRequest
*|　            -Handler. [UN-EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2022-06-28 20:09
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpHandler.h"
namespace WSHTTP {
	class HttpGetRequestHandler :public HttpHandler {
	private:
		const string TAG = "HttpGetRequestHandler";
	
	public:
		virtual const string& getTag() const override
		{
			return TAG;
		}

		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			if (request.METHOD.compare("GET") != 0) {
				Util::LOG(L_INFO, TAG, "Skip the request from METHOD:" + request.METHOD);
				return true;
			}
			//send the data
			BOOL ret = HttpSendRequestA(respCaller.request, request._req_cookies_string.c_str(), request._req_cookies_string.size(), NULL, 0);
			if (!ret) {
				DWORD error = GetLastError();
				respCaller.setCode(error);
				respCaller.setMsg("Send request failed. Error code:" + to_string(error));
				Util::LOG(L_ERROR, TAG, respCaller.getMsg());
				connection.releaseHandle(respCaller.request);
				return false;
			}
			return true;
		}

		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			return true;
		}
	};
};