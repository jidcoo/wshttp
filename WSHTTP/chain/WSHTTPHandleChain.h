/**
*-------------------------------------------------------------------------------------------------------------
*|��Description:WSHTTPHandleChain Class [http��������] [UN-EXPORTED] 
*|��Author: Jidcoo
*|��Version: 1.1
*|��Date: 2022-06-28 19:38
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|��[Update]
*|  Info: 
*|  Date: 
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "HttpHandler.h"
#include "../utils/Util.h"
namespace WSHTTP {
	class WSHTTPHandleChain :public HttpHandler{
	public:
		WSHTTPHandleChain();
		virtual ~WSHTTPHandleChain();
	private:
		virtual const string& getTag() const override;
		virtual bool handle(HttpMethod& request, HttpConnection& connection,
			ResponseCaller& respCaller, HttpHandler* *handlers, HttpHandler* chain) override;
		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override;
		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller)  override;
	private:
		friend class WSHttpRunner;
		const string TAG = "WSHTTPHandleChain";
		int handler;
		/**
		 * -1 error stop
		    0 ready
			1 finish
			2 handling
		 */
		int flag;
		void* _context_;
	private:
		bool isErrorStopped();
		bool isReady();
		bool isHandling();
		bool isFinished();
		void ready();
	};
};