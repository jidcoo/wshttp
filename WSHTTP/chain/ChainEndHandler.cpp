/**
*-------------------------------------------------------------------------------------------------------------
*|��Description:Http Request Exec Chain Node:ChainEndHandler [nothing to do on this handler] [UN-EXPORTED]
*|��Author:Jidcoo
*|��Version:1.0
*|��Date:2022-06-28 20:09
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|��[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpHandler.h"
namespace WSHTTP {
	class ChainEndHandler :public HttpHandler {
	private:
		const string TAG = "ChainEndHandler";
	private:

		virtual const string& getTag() const override
		{
			return TAG;
		}

		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			//Util::LOG(L_INFO, TAG, "WSHTTPHandleChain handle request completed");
			return true;
		}

		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			//Util::LOG(L_INFO, TAG, "WSHTTPHandleChain start handle response");
			return true;
		}

		virtual bool handle(HttpMethod& request, HttpConnection& connection,
			ResponseCaller& respCaller, HttpHandler* *handlers, HttpHandler* chain) override{
			beforeRequest(request, connection, respCaller);
			//do somethings����
			afterResponse(request, connection, respCaller);
			return true;
		}
	};

};