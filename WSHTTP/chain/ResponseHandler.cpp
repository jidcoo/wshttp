﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:Http Request Exec Chain Node:ResponseHandler [UN-EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2022-06-28 20:09
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpHandler.h"
namespace WSHTTP {
	class ResponseHandler :public HttpHandler {
	private:
		friend class WSHttpRunner;
		const string TAG = "ResponseHandler";
	private:
		DWORD queryHttpResponseState(DWORD &stateCode, string &stateMsg, HINTERNET request) {
			//query the http state code
			DWORD nStatusCode = 0;
			DWORD size = sizeof(nStatusCode);
			BOOL bRet = HttpQueryInfo(request, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, (LPVOID)&nStatusCode, &size, NULL);
			stateCode = nStatusCode;
			DWORD errCode = 0;
			if (!bRet) {
				errCode = GetLastError();
				Util::LOG(L_ERROR, TAG, "Query http status code failed. Error code:" + to_string(errCode) + "Http Status code:" + to_string(nStatusCode));
				return errCode;
			}
			//query the http state text
			char szBuffer[1024 + 1] = { 0 };
			DWORD dwReadSize = 1024;
			bRet = HttpQueryInfo(request, HTTP_QUERY_STATUS_TEXT, szBuffer, &dwReadSize, NULL);
			if (!bRet) {
				errCode = GetLastError();
				Util::LOG(L_ERROR, TAG, "Query http status text msg failed. Error code:" + to_string(errCode) + "Http Status code:" + to_string(nStatusCode));
				return errCode;
			}
			stateMsg = szBuffer;
			return 0;
		}
	private:
		virtual const string& getTag() const override
		{
			return TAG;
		}

		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			return true;
		}
		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			/* handle the http response state start */
			DWORD stateCode;
			string stateMsg;
			DWORD error = queryHttpResponseState(stateCode, stateMsg, respCaller.request);
			if (error) {
				respCaller.setCode(error);
				respCaller.setMsg("Query http status fail. Error code:" + to_string(error));
				Util::LOG(L_WARNNING, TAG, respCaller.getMsg());
				connection.releaseHandle(respCaller.request);
				return false;
			}
			if (stateCode < 200 || stateCode >= 300) {
				Util::LOG(L_WARNNING, TAG, "Get an abnormal http status code:" + to_string(stateCode));
			}
			respCaller.setCode(stateCode);
			respCaller.setMsg(stateMsg);
			if(request._cur_curl!=nullptr){
				respCaller.query = request._cur_curl->getPath();
			}
			return true;
		}
	};
};