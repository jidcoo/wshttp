﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:Http Request Exec Chain Node:RequestHandler [UN-EXPORTED]                                             
*|　Author:Jidcoo                                                
*|　Version:1.0                                                 
*|　Date:2022-06-28 20:09
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]                                                    
*|  Info:                                                        
*|  Date:                                                   
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpHandler.h"
namespace WSHTTP {
	class RequestHandler :public HttpHandler {
	protected:
		const string TAG = "RequestHandler";
	private:
		virtual const string& getTag() const override
		{
			return TAG;
		}


		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override  {
			if (connection.isClosed()) {
				Util::LOG(L_WARNNING, TAG, "The connection is closed");
				return false;
			}
			if (request._cur_curl==nullptr) {
				Util::LOG(L_ERROR, TAG, "The curl is null");
				return false;
			}
			request.setCURL(connection.__curl);
			LPCSTR defaultAcceptTypes[] = { LPCSTR("*/*"),NULL };
			//HTTP、HTTPS
			string HTTP_TAG("HTTP");
			string HTTPS_TAG("HTTPS");
			DWORD config = INTERNET_FLAG_RELOAD
				| INTERNET_FLAG_NO_UI| INTERNET_FLAG_NO_COOKIES;
			if (!connection.getConnectConfig().getRedirectsEnabled()) {
				config |= INTERNET_FLAG_NO_AUTO_REDIRECT;
			}else {
				config |=
					INTERNET_FLAG_IGNORE_REDIRECT_TO_HTTP |
					INTERNET_FLAG_IGNORE_REDIRECT_TO_HTTPS;
			}
			if (!connection.getConnectConfig().getKeepConnection()) {
				config |= INTERNET_FLAG_KEEP_CONNECTION;
			}
			if (!connection.getConnectConfig().getAuthenticationEnabled()) {
				config |= INTERNET_FLAG_NO_AUTH;
			}
			if (request._cur_curl->getUpperCastScheme() == HTTPS_TAG) {//HTTPS
				config |= INTERNET_FLAG_SECURE
					| INTERNET_FLAG_IGNORE_CERT_DATE_INVALID
					| INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
			}
			else {//For other cases use default HTTP, the upper layer connection will filter non TCP protocols
			}
			string http_version =
				connection.getConnectConfig().getHttpVersion() == Config::HTTP_VERSIONS::HTTP_1_0
				? "HTTP/1.0" : "HTTP/1.1";
			respCaller.request = HttpOpenRequestA(connection.mConnect, request.METHOD.c_str(),
				request._cur_curl->getPath().c_str(), http_version.c_str(), NULL, defaultAcceptTypes, config, NULL);
			if (respCaller.request == nullptr) {
				DWORD error = GetLastError();
				respCaller.setCode(error);
				respCaller.setMsg("Failed to open link. Error code:" + to_string(error));
				Util::LOG(L_ERROR, TAG, respCaller.getMsg());
				return false;
			}
			connection.setTimeoutConfigWithHandle(respCaller.request);
			return true;
		}

		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			return true;
		}
	};
};