/**
*-------------------------------------------------------------------------------------------------------------
*|��Description:WSHTTPHandleChain Class [http��������] [UN-EXPORTED] 
*|��Author: Jidcoo
*|��Version: 1.1
*|��Date: 2022-06-28 19:38
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|��[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "WSHTTPHandleChain.h"

WSHTTP::WSHTTPHandleChain::WSHTTPHandleChain()
{
	flag = 0;
	handler = 0;
}

WSHTTP::WSHTTPHandleChain::~WSHTTPHandleChain()
{
}

const string & WSHTTP::WSHTTPHandleChain::getTag() const
{
	return TAG;
}

bool WSHTTP::WSHTTPHandleChain::handle(HttpMethod & request, HttpConnection & connection,ResponseCaller& respCaller, HttpHandler* *handlers, HttpHandler* chain) {
	if (isReady() && handler == 0) flag = 2;
	if (
			!isErrorStopped() &&
			isHandling() &&
			!(handlers[handler++]->handle(request, connection, respCaller, handlers, this))
		)
	{
	    if (!isErrorStopped()) {
			Util::LOG(L_ERROR, TAG, "WSHTTPHandleChain handling error stopped!! [Handler(" + to_string(handler - 1) + ")]");
		}
		flag = -1;
		return false;
	}
	if (isErrorStopped()) {
		return false;
	}
	return true;
}

bool WSHTTP::WSHTTPHandleChain::beforeRequest(HttpMethod & request, HttpConnection & connection, ResponseCaller & respCaller)
{
	return true;
}

bool WSHTTP::WSHTTPHandleChain::afterResponse(HttpMethod & request, HttpConnection & connection, ResponseCaller & respCaller)
{
	return true;
}

bool WSHTTP::WSHTTPHandleChain::isErrorStopped()
{
	return flag==-1;
}

bool WSHTTP::WSHTTPHandleChain::isReady()
{
	return flag==0;
}

bool WSHTTP::WSHTTPHandleChain::isHandling()
{
	return flag==2;
}

bool WSHTTP::WSHTTPHandleChain::isFinished()
{
	return flag==1;
}

void WSHTTP::WSHTTPHandleChain::ready()
{
	flag = 0;
	handler = 0;
}


