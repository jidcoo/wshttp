/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:Http Request Exec Chain Node:HttpHeaderHandler [UN-EXPORTED]
*|　Author:Jidcoo
*|　Version:1.0
*|　Date:2022-06-28 20:09
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:
*|  Date:
*-------------------------------------------------------------------------------------------------------------
*/

#include "HttpHandler.h"
namespace WSHTTP {
	class HttpHeaderHandler :public HttpHandler {
	private:
		friend class WSHttpRunner;
		const string TAG = "HttpHeaderHandler";
	private:
		virtual const string& getTag() const override
		{
			return TAG;
		}

		virtual bool beforeRequest(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			if(request.getRequestHeaders().size()>0){
				string headers = "";
				headers = Util::parseMapDataToText(request.getRequestHeaders(), "\r\n", ": ");
				if (!HttpAddRequestHeadersA(respCaller.request, headers.c_str()
					, headers.size(), HTTP_ADDREQ_FLAG_ADD | HTTP_ADDREQ_FLAG_REPLACE)) {
					Util::LOG(L_WARNNING, TAG, "add Headers failed(" + to_string(GetLastError()) + "). [Headers: " + headers + "]");
				}
			}
			return true;
		}

		virtual bool afterResponse(HttpMethod& request, HttpConnection& connection, ResponseCaller& respCaller) override {
			LPVOID lpOutBuffer = NULL;
			DWORD dwSize = 0;
retry:
			// This call will fail on the first pass, because
			// no buffer is allocated.
			if (!HttpQueryInfoA(respCaller.request, HTTP_QUERY_RAW_HEADERS_CRLF,
				(LPVOID)lpOutBuffer, &dwSize, NULL)){
				if (GetLastError() == ERROR_HTTP_HEADER_NOT_FOUND){
					// Code to handle the case where the header isn't available.
					return true;
				}else{
					// Check for an insufficient buffer.
					if (GetLastError() == ERROR_INSUFFICIENT_BUFFER){
						// Allocate the necessary buffer.
						lpOutBuffer = new char[dwSize];
						// Retry the call.
						goto retry;
					}else{
						// Error handling code.
						if (lpOutBuffer){
							delete[] lpOutBuffer;
						}
						return false;
					}
				}
			}
			if (lpOutBuffer){
				parseHeadersRaw((char*)lpOutBuffer, request._cur_curl,respCaller);
				delete[] lpOutBuffer;
			}
			return true;
		}

		void parseHeadersRaw(char *headers, CURL* _curl,ResponseCaller& resp){
			stringstream strStream(headers);
			string item, key, val;
			string SET_COOKIE_TAG("SET-COOKIE");
			string CONTENT_LENGTH_TAG("CONTENT-LENGTH");
			int i;
			while (std::getline(strStream, item, '\n')) {
				i = item.find_first_of(":");
				if (i != -1) {
					key = item.substr(0, i);
					val = item.substr(i + 1);
					val=Util::trim(val);
					string tmp(key);
					Util::toUpperString(tmp);
					if (tmp == SET_COOKIE_TAG&&_curl!=nullptr){
						Cookie cookie = Cookie::parse(val, _curl->getHost(), _curl->getPath());
						if(!cookie.isInvalid()){
							resp.responseCookies.push_back(cookie);
						}
					}
					if(tmp == CONTENT_LENGTH_TAG){
						resp.setBodySize(stoull(val.c_str()));
					}
					resp.responseHeaders.insert(pair<string, string>(tmp, val));
				}
			}
		}
	};
};