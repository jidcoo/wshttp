﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:WSHTTP Connection Config [EXPORTED]
*|　Author:Jidcoo 
*|　Version:1.1 
*|　Date:2022-06-28 19:52  
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update] 
*|  Info:新增了超时控制属性及其方法，放弃对Proxy Socks的配置支持 
*|  Date:2022-06-28
*-------------------------------------------------------------------------------------------------------------
*/

#pragma once
#include "../utils/Util.h"
using namespace std;
namespace WSHTTP {
	typedef int WSHTTP_API time_unit_sec;
  	class WSHTTP_API Config {
	public:
		class WSHTTP_API HttpProxy{
		public:
		   typedef enum WSHTTP_API {
				HTTP,
				HTTPS,
				SOCKS4,
				SOCKS5,//Not supported.
			} PROXY_PROTOCOL;
		    HttpProxy(string hostname, int port, PROXY_PROTOCOL protocol = HTTP);
			const PROXY_PROTOCOL& getProtocol();
			const int& getPort();
			const string& getHostName();
			const string toString();
		private:
			string TAG = "HttpProxy";
		private:
			PROXY_PROTOCOL protocol;
			string hostName;
			int port;
		public:
			static HttpProxy getDefaultNullHttpProxy();
		};
	public:
		/**
 		 * 访问要求类型
		 */
		enum  ACCESS_TYPE {
			DIRECT,//解析所有本地主机
			PRECONFIG,//返回注册表中代理或直接的配置 默认
			PROXY//代理传递请求，除非代理提供了旁路列表且解析的名字可以绕过代理；此时，函数使用INTERNET_OPEN_TYPE_DIRECT。
		};
		enum  FLAG {  
			F_NULL,//默认
			ASYNC,//仅能用于作用在该函数返回的句柄的子句柄上的异步请求
			FROM_CACHE,//不做网络请求。所有的实体都由缓存返回。如果请求条目不在缓存中，一个适当的错误将返回。
		};
		enum HTTP_VERSIONS {
			HTTP_1_0,//HTTP 1.0版本
			HTTP_1_1 //HTTP 1.1版本
		};
		ACCESS_TYPE accessType;//访问类型
		FLAG flag;//访问标志
		string userAgent;//请求网络的用户标识
		HTTP_VERSIONS httpVersion;//使用的HTTP版本
		HttpProxy proxy;//代理类
		time_unit_sec connectTimeout;//连接超时限制
		time_unit_sec sendTimeout;//发送超时限制
		time_unit_sec responseTimeout;//响应超时限制
		bool redirectsEnabled;//是否支持自动重定向
		bool authenticationEnabled;//是否支持自动认证
		bool keepConnection;//保持连接
	public:
		Config();
		HTTP_VERSIONS getHttpVersion();
		void setHttpVersion(HTTP_VERSIONS v);
		ACCESS_TYPE getAccessType();
		void setAccessType(ACCESS_TYPE type);
		string getUserAgent();
		void setUserAgent(string userAgent);
		void setHttpProxy(HttpProxy& proxy);
		HttpProxy& getHttpProxy();
		FLAG getFlag();
		bool getRedirectsEnabled();
		void setRedirectsEnabled(bool redirectsEnabled);
		bool getAuthenticationEnabled();
		void setAuthenticationEnabled(bool authenticationEnabled);
		bool getKeepConnection();
		void setKeepConnection(bool keepConnection);
		time_unit_sec getConnectTimeout();
		void setConnectTimeout(time_unit_sec timeout);
		time_unit_sec getSendTimeout();
		void setSendTimeout(time_unit_sec timeout);
		time_unit_sec getResponseTimeout();
		void setResponseTimeout(time_unit_sec timeout);
		static Config getDefaultConfig();
	};
};

