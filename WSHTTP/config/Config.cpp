﻿/**
*-------------------------------------------------------------------------------------------------------------
*|　Description:WSHTTP Connection Config [EXPORTED]
*|　Author:Jidcoo
*|　Version:1.1
*|　Date:2022-06-28 19:52
*-------------------------------------------------------------------------------------------------------------
*-------------------------------------------------------------------------------------------------------------
*|　[Update]
*|  Info:新增了超时控制属性及其方法，放弃对Proxy Socks的配置支持
*|  Date:2022-06-28
*-------------------------------------------------------------------------------------------------------------
*/

#include "Config.h"

WSHTTP::Config WSHTTP::Config::getDefaultConfig(){
	//create the deafult connection config
	Config config;
	config.accessType = PRECONFIG;
	config.flag = F_NULL;
	config.connectTimeout = -1;
	config.sendTimeout = -1;
	config.responseTimeout = -1;
	config.redirectsEnabled = true;
	config.authenticationEnabled = false;
	config.keepConnection = true;
	config.httpVersion = HTTP_1_1;
	config.userAgent = "WSHTTP V1.0 Release";
	return config;
}

WSHTTP::Config::Config()
	:userAgent("WSHTTP V1.0 Release"), accessType(DIRECT),flag(F_NULL)
	,proxy(WSHTTP::Config::HttpProxy::getDefaultNullHttpProxy()){

}

WSHTTP::Config::HTTP_VERSIONS WSHTTP::Config::getHttpVersion()
{
	return this->httpVersion;
}

void WSHTTP::Config::setHttpVersion(HTTP_VERSIONS v)
{
	this->httpVersion = v;
}

WSHTTP::Config::ACCESS_TYPE WSHTTP::Config::getAccessType()
{
	return this->accessType;
}

void WSHTTP::Config::setAccessType(ACCESS_TYPE _type)
{
	this->accessType = _type;
}


WSHTTP::Config::FLAG WSHTTP::Config::getFlag(){
	return this->flag;
}

WSHTTP::time_unit_sec WSHTTP::Config::getConnectTimeout()
{
	return this->connectTimeout;
}

void WSHTTP::Config::setConnectTimeout(time_unit_sec timeout)
{
	if (timeout <= 0) timeout = -1;
	this->connectTimeout = timeout;
}

WSHTTP::time_unit_sec WSHTTP::Config::getSendTimeout()
{
	return this->sendTimeout;
}

void WSHTTP::Config::setSendTimeout(time_unit_sec timeout)
{
	if (timeout <= 0) timeout = -1;
	this->sendTimeout = timeout;
}

WSHTTP::time_unit_sec WSHTTP::Config::getResponseTimeout()
{
	return this->responseTimeout;
}

void WSHTTP::Config::setResponseTimeout(time_unit_sec timeout)
{
	if (timeout <= 0) timeout = -1;
	this->responseTimeout = timeout;
}

bool WSHTTP::Config::getRedirectsEnabled()
{
	return this->redirectsEnabled;
}

void WSHTTP::Config::setRedirectsEnabled(bool redirectsEnabled)
{
	this->redirectsEnabled = redirectsEnabled;
}

bool WSHTTP::Config::getAuthenticationEnabled()
{
	return this->authenticationEnabled;
}

void WSHTTP::Config::setAuthenticationEnabled(bool authenticationEnabled)
{
	this->authenticationEnabled = authenticationEnabled;
}

bool WSHTTP::Config::getKeepConnection()
{
	return this->keepConnection;
}

void WSHTTP::Config::setKeepConnection(bool keepConnection)
{
	this->keepConnection = keepConnection;
}

string WSHTTP::Config::getUserAgent(){
	return this->userAgent;
}

void WSHTTP::Config::setUserAgent(string userAgent)
{
	this->userAgent = userAgent;
}

void WSHTTP::Config::setHttpProxy(HttpProxy & proxy)
{
	this->proxy = proxy;
}

WSHTTP::Config::HttpProxy & WSHTTP::Config::getHttpProxy()
{
	return proxy;
}


WSHTTP::Config::HttpProxy::HttpProxy(const string hostname, const int port,PROXY_PROTOCOL protocol)
{
	if(hostname.empty()){
		throw "host name can not empty";
	}
	this->hostName = hostname;
	if(port<0||port>65535){
		throw "port invaild";
	}
	this->port = port;
	this->protocol = protocol;
}

const WSHTTP::Config::HttpProxy::PROXY_PROTOCOL& WSHTTP::Config::HttpProxy::getProtocol()
{
	return this->protocol;
}

const int& WSHTTP::Config::HttpProxy::getPort()
{
	return this->port;
}

const string& WSHTTP::Config::HttpProxy::getHostName()
{
	return this->hostName;
}

/**
 *  return format
 * 	HTTP://proxyserver:port
 * 	HTTPS://proxyserver:port
 *	SOCKS=proxyserver:port
 */
const string WSHTTP::Config::HttpProxy::toString()
{
	string NULL_FLAG("NULL");
	if(this->getHostName().compare(NULL_FLAG)==0){
		return "";
	}
	string str;
	string port_str = to_string(port);
	switch (getProtocol()){
	case HTTP:
		str = "HTTP://";
		break;
	case HTTPS:
		str = "HTTPS://";
		break;
	case SOCKS4:
		str = "SOCKS=";
	case SOCKS5:
		// i don't know how to do in this case?
        // may be not supported in winlet ? 07-12
		// str = "SOCKS5=";07-12
		// according to the offical thread,the socks5 is not supported.09-30
		str = "SOCKS=";
		Util::LOG(L_WARNNING, TAG, "the socks proxy is not supported in this library");
		//Util::LOG(L_WARNNING, TAG, "use socks4 instead of the socks5 protocol");
		return "";
	default:
		str = "HTTP://";
		break;
	}
	str += (hostName + ":");
	str += port_str;
	return str;
}

WSHTTP::Config::HttpProxy WSHTTP::Config::HttpProxy::getDefaultNullHttpProxy()
{
	HttpProxy proxy("NULL", 0,HTTP);
	return proxy;
}
